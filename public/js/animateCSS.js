function animateCSS(element, animationName) {
    const node = document.querySelector(element);
    node.classList.add('animated', animationName, "slow");

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName, "slow");
        node.removeEventListener('animationend', handleAnimationEnd);
    }

    node.addEventListener('animationend', handleAnimationEnd);
}