<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>X-Project :: Bootstrap Concept</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="panda eduProject X-Project ">
        <meta content="" name="design page for Laravel v.1">

        <!-- Favicons -->
        <link href="img/favicon.png" rel="icon">
        <link href="img/panda.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

        <!-- Bootstrap CSS File -->
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="lib/animate/animate.min.css" rel="stylesheet">
        <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
        <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

        <!-- Main Stylesheet File -->
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <!--==========================
        Header
        ============================-->
        <header id="header">
            <div class="container-fluid">

            <div id="logo" class="pull-left">
                <!-- Uncomment below if you prefer to use an image logo -->
                <h1><a href="#intro"><img class="" width="40px" height="40px" src="img/logo_sman2_bandung_2.png" alt="X-Project" title="X-Project"/> SMAN 2 Bandung</a></h1>
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                <li class="menu-active"><a href="#intro">Home</a></li>
                <li><a href="#about">Sekapur Sirih</a></li>
                <li><a href="#services">Ruang Lingkup</a></li>
                <li><a href="#portfolio">Portofolio</a></li>
                <li><a href="#team">Team</a></li>
                <li class="menu-has-children"><a href="">X-Project</a>
                    <ul>
                    <li><a href="http://10.10.11.10/eduOnepage" target="_blank">eduOnePage</a></li>
                    <li><a href="#">Charets Admin Panel</a></li>
                    <li><a href="#">DFD Center</a></li>
                    <li><a href="#">ERD Center</a></li>
                    <li><a href="http://10.10.11.10:3445/admin" target="_blank">eduProject - Charets</a></li>
                    <li><a href="#">e-commerce Model</a></li>
                    <li><a href="#">SIMDIK</a></li>
                    <li><a href="http://10.10.11.10:8787/" target="_blank">Charets Shop</a></li>
                    </ul>
                </li>
                <li><a href="#contact">Saran & Kritik</a></li>
                </ul>
            </nav><!-- #nav-menu-container -->
            </div>
        </header><!-- #header -->

        <main class="pt-4">
            @yield('content')
        </main>
    </body>
</html>