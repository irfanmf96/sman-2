<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SMAN 2 BANDUNG</title>

    <!-- Scripts -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <script src="{{ asset('succinct-master/jQuery.succinct.min.js') }}" defer></script>

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js" defer></script>

    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" defer></script>

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>


    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('fontawesome/css/all.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    

</head>
<body id="main">
    <div id="app">
        <nav class="py-0 navbar fixed-top navbar-expand-md navbar-light">
            <div class="container-fluid">
                <a class="navbar-brand pr-4" href="{{ url('/') }}">
                    <img id="logo-navbar" src="{{ asset('/img/Logo_SMAN2.png') }}" alt="Logo SMANDA dengan tulisan SMAN 2 Bandung">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        @if(Request::is('/'))
                            <a class="selected nav-link" href="{{ route('home') }}">Beranda</a>
                        @else
                        <a class="nav-link" href="{{ route('home') }}">Beranda</a>
                        @endif
                        </li>
                        <li class="nav-item">
                        @if(Request::is('news/*') or Request::is('news'))
                            <a class="selected nav-link" href="{{ route('news') }}">Berita</a>
                        @else
                            <a class="nav-link" href="{{ route('news') }}">Berita</a>
                        @endif
                        </li>
                        <li class="nav-item dropdown">
                        @if(Request::is('profile/*'))
                            <a class="selected nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Profil
                            </a>
                        @else
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Profil
                            </a>
                        @endif
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('history') }}">Sejarah Singkat</a>
                                <a class="dropdown-item" href="{{ route('vision_mission') }}">Visi dan Misi</a>
                                <a class="dropdown-item" href="{{ route('facility') }}">Sarana dan Prasarana</a>
                                <a class="dropdown-item" href="{{ route('education_staff') }}">Tenaga Pendidik</a>
                                <a class="dropdown-item" href="{{ route('contact') }}">Kontak dan Alamat</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                        @if(Request::is('students/*'))
                            <a class="selected nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Siswa dan Alumni
                            </a>
                        @else
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Siswa dan Alumni
                            </a>
                        @endif
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('excul') }}">Ekstrakurikuler</a>
                                <a class="dropdown-item" href="{{ route('osis') }}">OSIS</a>
                                <a class="dropdown-item" href="{{ route('mpk') }}">MPK</a>
                                <a class="dropdown-item" href="{{ route('students_alumni_list') }}">Direktori Siswa dan Alumni</a>
                            </div>
                        </li>
                        <li class="nav-item">
                        @if(Request::is('gallery/*') or Request::is('gallery'))
                            <a class="selected nav-link" href="{{ route('gallery') }}">Galeri</a>
                        @else
                            <a class="nav-link" href="{{ route('gallery') }}">Galeri</a>
                        @endif
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login-page') }}">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="pt-4">
            @yield('content')
        </main>
        <footer class="footer">
            <div class="container-fluid">
                <div class="row alt-pallate d-flex justify-content-center color-smanda-alt">
                    <a class="pr-3"><i class="fas fa-home"></i> Jalan Cihampelas No. 173 Bandung 40131</a>
                    <a class="pr-3"><i class="fas fa-phone"></i> <i class="fas fa-fax"></i> +62-22-2032462</a>
                    <a class="pr-3"><i class="fas fa-envelope"></i> Contact@sman2bdg.sch.id</a>
                </div>
                <div class="row alt-pallate-copyright d-flex justify-content-center  color-smanda-alt pl-2">
                    <a><b>Copyright © 2020 - SMAN 2 Bandung.</b> All rights reserved.</a>
                </div>
            </div>
        </footer>
    </div>
</body>
<script>
    AOS.init();
    $(document).ready( function () {
        $('.DataTables').DataTable();
        $('.truncate').succinct({
            size: 460
        });
    } );
</script>
</html>

