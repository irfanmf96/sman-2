@extends('layouts.app')

@section('content')
<div class="container content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row test d-flex justify-content-center">
        <i class="fas fa-user color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Daftar Siswa</h2>
    </div>
    <!--<div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
    <button type="button" onclick="animateCSS()" class="btn btn-primary">Primary</button>
    </div>-->
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row pt-4 d-flex justify-content-center">
        <div class="col-md-10 col-sm-12 hscroll">
            <table class="table DataTables table-striped" style="width:100%">
                <thead class="table-komite">
                    <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Pelajaran</th>
                        <th>Tugas</th>
                        <th>Kelamin</th>
                    </tr>
                </thead>
                <tbody>
                <!-- Hapus {{-- dan --}} serta data dummy saat implimentasi -->
                {{--@foreach($gurus as $guru)--}}
                    <tr>
                        <td>{{--{{$guru->NIP}}--}} 100000008</td>
                        <td>{{--{{$guru->Nama}}--}} Kusnadi, S.Pdt</td>
                        <td>{{--{{$guru->Mapel}}--}} Bahasa Sunda</td>
                        <td>{{--{{$guru->Jabatan}}--}} Guru</td>
                        <td>{{--{{$guru->Jeniskelamin}}--}} L</td>
                    </tr>
                {{--@endforeach--}}
                </tbody>
            </table>
        </div>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row pt-5 d-flex justify-content-center"><i class=""></i>
        <i class="fas fa-user-graduate color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Daftar Alumni</h2>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row py-4 d-flex justify-content-center">
        <div class="col-md-10 col-sm-12 hscroll">
            <table class="color-success table DataTables table-striped" style="width:100%">
                <thead class="table-komite">
                    <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Pelajaran</th>
                        <th>Tugas</th>
                        <th>Kelamin</th>
                    </tr>
                </thead>
                <tbody>
                <!-- Hapus {{-- dan --}} serta data dummy saat implimentasi -->
                {{--@foreach($gurus as $guru)--}}
                    <tr>
                        <td>{{--{{$guru->NIP}}--}} 100000008</td>
                        <td>{{--{{$guru->Nama}}--}} Kusnadi, S.Pdt</td>
                        <td>{{--{{$guru->Mapel}}--}} Bahasa Sunda</td>
                        <td>{{--{{$guru->Jabatan}}--}} Guru</td>
                        <td>{{--{{$guru->Jeniskelamin}}--}} L</td>
                    </tr>
                {{--@endforeach--}}
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function animateCSS() {
        const node = document.querySelector(".test");
        node.classList.add('animated', 'fadeIn', 'slower');

        function handleAnimationEnd() {
            node.classList.remove('animated', 'fadeIn');
            node.removeEventListener('animationend', handleAnimationEnd);

        }

    node.addEventListener('animationend', handleAnimationEnd);
    }
</script>
@endsection