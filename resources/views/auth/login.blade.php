<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin Log-In SMAN 2 Bandung</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{asset('css/login.css')}}" rel="stylesheet">
</head>
<body>
    <div class="container" style="height:100vh">
        <div class="row-align-items-center" >
            <div class="col md-8 mr-auto ml-auto">
                <div class="card bg-light">
                    <div class="card header">
                        <div style="text-align: center">
                            <img src="\img\logo_sman2_bandung.png" width= ""style="height:s">
                        </div>
                    <div class="row-align-item-center">
                        <div class="col md-6 offset-md-3">
                            <form method="POST" action="{{ route('login-admin') }}">
                                @csrf
                                <div class="row-align-item-center">
                                    <div class="col-6 form-group">
                                        <label>Email</label>
                                        <input class="form-control" type="email" name="email" placeholder="email" value required>
                                    </div>
                                </div>
                                <div class="row-align-item-center">
                                    <div class="col-6 form-group">
                                        <label>Password</label>
                                        <input class="form-control" type="password" name="password" placeholder="password" value required>
                                    </div>
                                </div>
                                <div class="row-align-item-center">
                                    <div class="col-6 form-group">
                                        <button type="submit" class="btn btn-success btn-block">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>