@extends('admin.layout.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{route('staff')}}">Daftar Staff</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>

<div class="row mt-3 ml-1">
    <div class="col-sm-6">
        <button type="button" class="btn btn-primary" id="add-staff" title="add staff">
            <i class="fa fa-plus" aria-hidden="true"></i> Add staff
        </button>
    </div>
</div>

<div class="card bg-light ml-3 mt-3">
  <div class="card-body">
      <table  id="staffTable" class="datatable table" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Nama</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($staff as $staff_)
            <tr>
                
                <td scope="col">{{$staff_->first_name}} {{$staff_->last_name}}</td>
                <td scope="col">
                  <button value = "{{$staff_->id}}" class="btn btn-success btn-md" id="show">Detail</button>
                  <a href = "{{route('staff.edit',$staff_->id)}}" class="btn btn-info btn-md">Edit</a>
                  <a href = "{{route('staff.destroy',$staff_->id)}}" class="btn btn-danger btn-md">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
  </div>
</div>
@endsection

@section('script')
<script>
    $('#add-staff').on( 'click', function (e) {
        e.preventDefault();
        location.href = "staff/create";
    });
    $('#edit').on('click',function(e){
        e.preventDefault();
        var staff_id = $('#edit').val();
        location.href = "staff/edit/"+staff_id;
    });
    $('#delete').on('click',function(e){
        e.preventDefault();
        var staff_id = $('#delete').val();
        location.href = "staff/delete/"+staff_id;
    });
    $(document).ready( function () {
    $('#staffTable').DataTable();
    });
</script>
@endsection