@csrf
@if(isset($model))
<input type="hidden" name="staff_id" value="{{$model->id}}">
@endif

<div class="card card-primary">
    <form>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="nomor_induk_staff"> NIK </label>
                        <input type="text" class="form-control  {!! $errors->has('nomor_induk_staff') ? 'is-invalid' : '' !!}" id="nomor_induk_staff" name="nomor_induk_staff" placeholder="Masukan Nomor Induk staff" value="{{isset($model) ? $model->pin : old('nomor_induk_staff') }}">
                        {!! $errors->first('nomor_induk_staff', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="nama_depan"> Nama Depan </label>
                        <input type="text" class="form-control  {!! $errors->has('nama_depan') ? 'is-invalid' : '' !!}" id="nama_depan" name="nama_depan" placeholder="Masukan Nama Depan" value="{{isset($model) ? $model->first_name : old('nama_depan') }}">
                        {!! $errors->first('nama_depan', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="nama_belakang"> Nama Belakang </label>
                        <input type="text" class="form-control  {!! $errors->has('nama_belakang') ? 'is-invalid' : '' !!}" id="nama_belakang" name="nama_belakang" placeholder="Masukan Nama Belakang" value="{{isset($model) ? $model->last_name : old('nama_belakang') }}">
                        {!! $errors->first('nama_belakang', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="pas-foto">Pas Foto</label>
                <input type="file" class="form-control-file" id="pas-foto" name="pas-foto">
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="alamat"> Alamat </label>
                        <input type="text" class="form-control  {!! $errors->has('alamat') ? 'is-invalid' : '' !!}" id="alamat" name="alamat" placeholder="Masukan Alamat" value="{{isset($model) ? $model->address : old('alamat') }}">
                        {!! $errors->first('alamat', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="tempat_lahir"> Tempat Lahir </label>
                        <input type="text" class="form-control  {!! $errors->has('tempat_lahir') ? 'is-invalid' : '' !!}" id="tempat_lahir" name="tempat_lahir" placeholder="Masukan Tempat Lahir" value="{{isset($model) ? $model->birth_place : old('tempat_lahir') }}">
                        {!! $errors->first('alamat', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label for="tanggal_lahir">Tanggal Lahir</label>
                    <input type="date" name="tanggal_lahir" class="form-control {!! $errors->has('tanggal_lahir') ? 'is-invalid' : '' !!}" id="tanggal_lahir" placeholder="Masukan Tanggal Lahir" value="{{isset($model) ? $model->birth_date : old('tanggal_lahir') }}">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="email"> Email </label>
                        <input type="text" class="form-control  {!! $errors->has('email') ? 'is-invalid' : '' !!}" id="email" name="email" placeholder="Masukan email" value="{{isset($model) ? $model->email : old('email') }}">
                        {!! $errors->first('email', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="mobile_phone"> Nomor Handphone </label>
                        <input type="text" class="form-control  {!! $errors->has('mobile_phone') ? 'is-invalid' : '' !!}" id="mobile_phone" name="mobile_phone" placeholder="Masukan Nomor Hp" value="{{isset($model) ? $model->mobile_phone : old('mobile_phone') }}">
                        {!! $errors->first('mobile_phone', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select class="form-control select2-single" id="jenis_kelamin" name="jenis_kelamin">
                            @foreach($sex as $sex_)
                            <option value="{{$sex_->id}}">{{$sex_->sex_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="agama">Agama</label>
                        <select class="form-control select2-single" id="agama" name="agama">
                            @foreach($religion as $religion_)
                            <option value="{{$religion_->id}}">{{$religion_->religion_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="jabatan">Jabatan</label>
                        <select class="form-control select2-single" id="jabatan" name="jabatan">
                            @foreach($position as $position_)
                            <option value="{{$position_->id}}">{{$position_->position_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>