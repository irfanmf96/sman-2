@extends('admin.layout.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{route('student')}}">Daftar Murid</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>

<div class="row mt-3 ml-1">
    <div class="col-sm-6">
        <button type="button" class="btn btn-primary" id="add-student" title="add student">
            <i class="fa fa-plus" aria-hidden="true"></i> Add Student
        </button>
    </div>
</div>

<div class="card bg-light ml-3 mt-3">
  <div class="card-body">
      <table  id="muridTable" class="datatable table" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Nama</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($student as $student_)
            <tr>
                <td scope="col">{{$student_->first_name}} {{$student_->last_name}}</td>
                <td scope="col">
                  <button value="{{$student_->id}}" class="btn btn-success btn-md">Detail</button>
                  <a href="{{route('student.edit',$student_->id)}}" class="btn btn-info btn-md">Edit</a>
                  <a href="{{route('student.destroy',$student_->id)}}" class="btn btn-danger btn-md">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
  </div>
</div>
@endsection

@section('script')
<script>
    $('#add-student').on( 'click', function (e) {
        e.preventDefault();
        location.href = "student/create";
    });
    $('#edit').on('click',function(e){
        e.preventDefault();
        var student_id = $('#edit').val();
        location.href = "student/edit/"+student_id;
    });
    $(document).ready( function () {
    $('#muridTable').DataTable();
} );
</script>
@endsection