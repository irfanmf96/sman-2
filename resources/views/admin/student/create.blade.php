@extends('admin.layout.app')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="dashboard-widget-content">
                    <div class="card-header">
                        <h3 class="card-title">Formulir Penambahan Data Murid</h3>
                    </div>
                    <form class="form-horizontal form-label-left input_mask" role="form" method="POST" action="{{route('student.store')}}" enctype="multipart/form-data">
                        @include('admin.student.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection