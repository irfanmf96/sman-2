@extends('admin.layout.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{route('teacher')}}">Daftar Guru</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>

<div class="row mt-3 ml-1">
    <div class="col-sm-6">
        <button type="button" class="btn btn-primary" id="add-teacher" title="add teacher">
            <i class="fa fa-plus" aria-hidden="true"></i> Add Teacher
        </button>
    </div>
</div>

<div class="card bg-light ml-3 mt-3">
  <div class="card-body">
      <table  id="teacherTable" class="datatable table" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Nama</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($teacher as $teacher_)
            <tr>
                
                <td scope="col">{{$teacher_->first_name}} {{$teacher_->last_name}}</td>
                <td scope="col">
                  <button value= "{{$teacher_->id}}" class="btn btn-success btn-md" id="show">Detail</button>
                  <a href="{{route('teacher.edit',$teacher_->id)}}" class="btn btn-info btn-md">Edit</a>
                  <a href="{{route('teacher.destroy',$teacher_->id)}}" class="btn btn-danger btn-md">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
  </div>
</div>
@endsection

@section('script')
<script>
    $('#add-teacher').on( 'click', function (e) {
        e.preventDefault();
        location.href = "teacher/create";
    });
    $('#edit').on('click',function(e){
        e.preventDefault();
        var teacher_id = $('#edit').val();
        location.href = "teacher/edit/"+teacher_id;
    });
    $('#delete').on('click',function(e){
        e.preventDefault();
        var teacher_id = $('#delete').val();
        location.href = "teacher/delete/"+teacher_id;
    });
    $(document).ready( function () {
    $('#teacherTable').DataTable();
    });
</script>
@endsection