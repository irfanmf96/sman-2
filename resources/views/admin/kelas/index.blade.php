@extends('admin.layout.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{route('teacher')}}">Daftar Ruang Kelas</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>

<div class="row mt-3 ml-1">
    <div class="col-sm-6">
        <button type="button" class="btn btn-primary" id="add-class-room" title="add class room">
            <i class="fa fa-plus" aria-hidden="true"></i> Tambah Ruang Kelas
        </button>
    </div>
</div>

<div class="card bg-light ml-3 mt-3">
  <div class="card-body">
      <table  id="classTable" class="datatable table" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Nama Kelas</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($kelas as $kelas_)
            <tr>
                <td scope="col">{{$kelas_->grade}} {{$kelas_->name}}</td>
                <td scope="col">
                  <a href="{{route('kelas.edit',$kelas_->id)}}" class="btn btn-info btn-md">Edit</a>
                  <a href="{{route('kelas.destroy',$kelas_->id)}}" class="btn btn-danger btn-md">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
  </div>
</div>
@endsection

@section('script')
<script>
    $('#add-class-room').on( 'click', function (e) {
        e.preventDefault();
        location.href = "kelas/create";
    });
    $('#edit').on('click',function(e){
        e.preventDefault();
        var class_id = $('#edit').val();
        location.href = "class/edit/"+class_id;
    });
    $('#delete').on('click',function(e){
        e.preventDefault();
        var class_id = $('#delete').val();
        location.href = "class/delete/"+class_id;
    });
    $(document).ready( function () {
    $('#classTable').DataTable();
    });
</script>
@endsection