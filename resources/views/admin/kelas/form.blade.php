@csrf
@if(isset($model))
<input type="hidden" name="guru_id" value="{{$model->id}}">
@endif

<div class="card card-primary">
    <form>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="grade_class">Pilih Tingkatan</label>
                        <select class="form-control" id="grade_class" name="grade_class">
                            <option value="">-- Choose --</option>
                            <option {{ old('grade_class') == 10 ? 'selected' : '' }}>10</option>
                            <option {{ old('grade_class') == 11 ? 'selected' : '' }}>11</option>
                            <option {{ old('grade_class') == 12 ? 'selected' : '' }}>12</option>
                        </select>
                        {!! $errors->first('grade_class', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                    <div class="form-group">
                        <label for="name_class">Nama Kelas</label>
                        <input type="text" class="form-control {!! $errors->has('name_class') ? 'is-invalid' : '' !!}" id="name_class" name="name_class" value="{{isset($model) ? $model->name : old('name_class') }}">
                            {!! $errors->first('name_class', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                        </div>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
