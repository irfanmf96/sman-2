<!DOCTYPE html>

<html lang="en">
  <head>
    <style>
        body {
            display: flex;
            background-color: #F4F7FC !important;
            height: 100vh !important;
        }
        .sidebar {
            background-color: #2B3E50 !important;
            padding: 0;
            width: 280px !important;
            height: 100% !important;
            position: fixed;
            transition: 0.5s;
            overflow: hidden !important;
        }
        .sidebar:hover {
            overflow-y: scroll !important;
        }
        .sidebar-shrink {
            width: 100px !important;
            transition: 0.25s;
        }
        .content-container {
            /* width: calc(100% - 280px); */
            margin-left: 280px;
            transition: 0.25s;
            height: 100%;
        }
        .content-container-shrink {
            /* width: calc(100% - 100px); */
            margin-left: 100px;
            transition: 0.5s;
        }
        /* width */
        ::-webkit-scrollbar {
        width: 5px;
        }
        /* Track */
        ::-webkit-scrollbar-track {
        background: #f1f1f1;
        }
        /* Handle */
        ::-webkit-scrollbar-thumb {
        background: #888;
        }
        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
        background: #555;
        }
    </style>
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>Admin SMAN-2 Bandung</title>
    <!-- Icons-->
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{url('asset/admin/modules/@coreui/icons/css/coreui-icons.min.css')}}" rel="stylesheet">
    <link href="{{url('asset/admin/modules/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet">
    <link href="{{url('asset/admin/modules/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{url('asset/admin/modules/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Main styles for this application-->
    <link href="{{url('asset/toastjs/toast.min.css')}}" rel="stylesheet">
    <link href="{{url('asset/alertify/alertify.core.css')}}" rel="stylesheet">
    <link href="{{url('asset/alertify/alertify.bootstrap.css')}}" rel="stylesheet">
    <link href="{{url('asset/admin/vendors/pace-progress/css/pace.min.css')}}" rel="stylesheet">
    <link href="{{url('datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="{{url('asset/admin/modules/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{url('asset/admin/modules/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{url('asset/toastjs/toast.min.js')}}"></script>
    <script src="{{url('asset/alertify/alertify.min.js')}}"></script>
    <script src="{{url('datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    {{--<script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>--}}
    @yield('css')
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
  </head>

    <body class="sidebar-lg-show">
        <div class="w-100">
            @include('admin.layout.sidebar')
            <div class="content-container">
                @yield('content')
            </div>
        </div>


    <!-- CoreUI and necessary plugins-->
    <script src="{{url('asset/admin/modules/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{url('asset/admin/modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{url('asset/admin/modules/pace-progress/pace.min.js')}}"></script>
    <script src="{{url('asset/admin/modules/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
    <script src="{{url('asset/admin/modules/@coreui/coreui/dist/js/coreui.min.js')}}"></script>
    <!-- Plugins and scripts required by this view-->
    <script src="{{url('asset/admin/modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js')}}"></script>
    {{-- toast --}}
    <script>
        @if(Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
        @endif

        @if(Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
        @endif

        @if(Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
        @endif


        @if(Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @endif
    </script>

    {{-- Script Sidebar Wide --}}
    <script>
        var wideSidebar = true;
        function sidebarWide() {
            wideSidebar = !wideSidebar;
            if (wideSidebar == false) {
                $(".sidebar").addClass("sidebar-shrink");
                $(".content-container").addClass("content-container-shrink");
                $(".img-admin").addClass("img-admin-shrink");
                $(".text-admin").addClass("text-admin-shrink");
                $(".sidebar-menu").addClass("sidebar-menu-shrink");
                $(".sidebar-img").addClass("sidebar-img-shrink");
                $(".sidebar-text").addClass("sidebar-text-shrink");
                $(".burger-img").addClass("burger-img-shrink");
            } else {
                $(".sidebar").removeClass("sidebar-shrink");
                $(".content-container").removeClass("content-container-shrink");
                $(".img-admin").removeClass("img-admin-shrink");
                $(".text-admin").removeClass("text-admin-shrink");
                $(".sidebar-menu").removeClass("sidebar-menu-shrink");
                $(".sidebar-img").removeClass("sidebar-img-shrink");
                $(".sidebar-text").removeClass("sidebar-text-shrink");
                $(".burger-img").removeClass("burger-img-shrink");
            }
        }

    </script>
    {{-- Script Sidebar Wide --}}
    <script>
    //Timepicker
    $('.timepicker').timepicker({
        showInputs: false
    })
    </script>
    @yield('script')
  </body>
</html>