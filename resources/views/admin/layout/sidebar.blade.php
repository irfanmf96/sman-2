<style>
  .sidebar-content {
    width: 100%;
  }

  .burger-img {
    width: 24px;
    position: absolute;
    top: 28px;
    right: 24px;
    cursor: pointer;
  }

  .burger-img-shrink {
    display: block;
    top: 28px;
    right: 0;
    left: 0;
    margin: auto;
  }

  .sidebar-head {
    width: 100%;
    text-align: center;
    margin-top: 70px;
  }

  .img-admin {
    width: 75px;
    height: 75px;
    border-radius: 50%;
    display: block;
    margin: auto;
  }

  .img-admin-shrink {
    width: 36px;
    height: 36px;
    margin-bottom: 50px;
  }

  .text-admin {
    margin-top: 23px;
    margin-bottom: 34px;
    font-weight: bold;
    font-size: 24px;
    line-height: 42px;
    color: #FFFFFF;
  }

  .text-admin-shrink {
    display: none;
  }

  .sidebar-menu {
    display: flex;
    align-items: center;
    height: 60px;
    width: 100%;
    padding: 0 50px;
  }

  .sidebar-menu-shrink {
    padding: 0;
  }

  .sidebar-menu.active {
    background-color: rgba(255, 255, 255, 0.08);
    background: linear-gradient(to right, #FFB600 5%, rgba(255, 255, 255, 0.08) 0%);
  }

  .sidebar-menu:hover {
    background-color: rgba(255, 255, 255, 0.08);
    text-decoration: none;
  }

  .sidebar-img {
    width: 32px;
    margin-right: 15px;
  }

  .sidebar-img-shrink {
    display: block;
    margin: auto;
  }

  .sidebar-text {
    font-weight: normal;
    font-size: 14px;
    line-height: 16px;
    color: #FFFFFF;
    margin-bottom: 0;
  }

  .sidebar-text-shrink {
    display: none;
  }
</style>
<div class="sidebar">
  <div class="sidebar-content">
    <img class="burger-img" src="{{url('/asset/img/burger.svg')}}" onclick="sidebarWide()">
    <div class="sidebar-head">
      <img class="img-admin" src="{{url('/asset/admin/images_default/default.png')}}">
      <h1 class="text-admin">Admin</h1>
    </div>
    <a class="sidebar-menu">
      <img class="sidebar-img" src="{{url('/asset/img/dashboard.svg')}}">
      <h1 class="sidebar-text">Dashboard</h1>
    </a>
    <a class="sidebar-menu" href="{{route('blog')}}">
      <img class="sidebar-img" src="{{url('/asset/img/berita.svg')}}">
      <h1 class="sidebar-text">Berita</h1>
    </a>
    <a class="sidebar-menu" href="{{route('student')}}">
      <img class="sidebar-img" src="{{url('/asset/img/manage.svg')}}">
      <h1 class="sidebar-text">Data Murid</h1>
    </a>
    <a class="sidebar-menu" href="{{route('teacher')}}">
      <img class="sidebar-img" src="{{url('/asset/img/manage.svg')}}">
      <h1 class="sidebar-text">Data Guru</h1>
    </a>
    <a class="sidebar-menu" href="{{route('staff')}}">
      <img class="sidebar-img" src="{{url('/asset/img/manage.svg')}}">
      <h1 class="sidebar-text">Data Staff</h1>
    </a>
    <a class="sidebar-menu" href="#">
      <img class="sidebar-img" src="{{url('/asset/img/folder.svg')}}">
      <h1 class="sidebar-text">Kelas</h1>
    </a>
    <a class="sidebar-menu">
      <img class="sidebar-img" src="{{url('/asset/img/help.svg')}}">
      <h1 class="sidebar-text">Help Center</h1>
    </a>
   
    <a class="sidebar-menu" style="margin-top: 75px;" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        <img class="sidebar-img" src="{{url('/asset/img/logout.svg')}}">
        <h1 class="sidebar-text">Logout</h1>
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
  </div>
</div>

<script>
  $(document).ready(function() {
    var v = window.location.href;
    $(".sidebar .sidebar-content a").each(function() {
        var href = $(this).attr("href");
        if (v.includes(href)) {
          $(this).addClass('active')
        } else {
            $(this).removeClass("active");
        }
    });
});
</script>
