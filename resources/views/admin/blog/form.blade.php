@csrf
@if(isset($model))
<input type="hidden" name="blog_id" value="{{$model->id}}">
@endif

<div class="card card-primary">
    <form>
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="header-image">Header Image</label>
                        <input type="file" class="form-control-file" id="header-image" name="header-image">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group ml-5">
                        <label for="attachment">Dokumen</label>
                        <input type="file" class="form-control-file" id="attachment" name="attachment">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <label for="title"> Title </label>
                    <input type="text" class="form-control  {!! $errors->has('title') ? 'is-invalid' : '' !!}" id="title" name="title" value="{{isset($model) ? $model->title : old('title') }}">
                        {!! $errors->first('title', '<div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            :message
                    </div>') !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group my-3">
                        <label for="content">Content</label>
                        <textarea class="form-control {!! $errors->has('content') ? 'is-invalid' : '' !!}" id="content" rows="8" name="content">{{isset($model) ? $model->content : old('content') }}</textarea>
                    </div>
                </div>
            </div>
            <div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>