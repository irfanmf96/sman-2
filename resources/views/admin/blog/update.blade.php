@extends('admin.layout.app')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="dashboard-widget-content">
                    <div class="card-header">
                        <h3 class="card-title">Formulir Ubah Post</h3>
                    </div>
                    <form class="form-horizontal form-label-left input_mask" role="form" method="POST" action="{{route('blog.update')}}" enctype="multipart/form-data">
                        @include('admin.blog.form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection