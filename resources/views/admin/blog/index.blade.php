@extends('admin.layout.app')

@section('content')
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="{{route('blog')}}">Daftar Blog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>

<div class="row mt-3 ml-1">
    <div class="col-sm-6">
        <button type="button" class="btn btn-primary" id="add-blog" title="add-blog">
            <i class="fa fa-plus" aria-hidden="true"></i> Add new post
        </button>
    </div>
</div>

<div class="card bg-light ml-3 mt-3">
  <div class="card-body">
      <table  id="blogtable" class="datatable table" style="width:100%">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Judul</th>
                <th scope="col">Konten</th>
                <th scope="col">Preview Image</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            @foreach($blog as $blog_)
                <td scope="col">{{$blog_->title}}</td>
                <td scope="col">{{$blog_->content}}</td>
                <td scope="col"><img height="200px" width="200px" src="{{$blog_->header_image}}" scope="col"></td>
                <td scope="col">
                  <a href="{{route('blog.edit',$blog_->id)}}" class="btn btn-info btn-md">Edit</a>
                  <a href = "{{route('blog.destroy',$blog_->id)}}" class="btn btn-danger btn-md">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready( function () {
    $('#blogtable').DataTable();
  });
  $('#edit').on('click',function(e){
        e.preventDefault();
        var blog_id = $('#edit').val();
        location.href = "blog/edit/"+blog_id;
    });
    $('#delete').on('click',function(e){
        e.preventDefault();
        var teacher_id = $('#delete').val();
        location.href = "blog/delete/"+teacher_id;
    });
  $('#add-blog').on( 'click', function (e) {
        e.preventDefault();
        location.href = "blog/create";
  });
</script>
@endsection