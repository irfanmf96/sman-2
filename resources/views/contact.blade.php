@extends('layouts.app')

@section('content')
<div class="container-fluid content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <i class="far fa-address-book color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Kontak</h2>
    </div>
    <div class="row pt-4 d-flex justify-content-center">
        <div class="animated flipInY slow col-sm-12 col-md-6 col-lg-4 pb-4">
            <div class="card alt-pallate">
                <div class="card-body color-smanda-alt">
                    <h5 class="card-title font-weight-bold title-underline-alt"><i class="fas fa-phone pr-1"></i>No.Telp/Fax</h5>
                    <p class="card-text text-justify">+62-22-2032462</p>
                </div>
            </div>
        </div>
        <div class="animated flipInY slow col-sm-12 col-md-6 col-lg-4 pb-4">
            <div class="card alt-pallate">
                <div class="card-body color-smanda-alt">
                    <h5 class="card-title font-weight-bold title-underline-alt"><i class="fas fa-envelope pr-1"></i>Email</h5>
                    <p class="card-text text-justify">Contact@sman2bdg.sch.id</p>
                </div>
            </div>
        </div>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="pt-4 row d-flex justify-content-center">
        <i class="fas fa-map-marker-alt color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Lokasi</h2>
    </div>
    <div class="row pt-4 d-flex justify-content-center">
        <div class="animated flipInY slow col-sm-12 col-md-6 col-lg-6 pb-3">
            <div class="card alt-pallate">
                <div class="card-body color-smanda-alt">
                    <h5 class="card-title font-weight-bold title-underline-alt"><i class="fas fa-home pr-1"></i>Alamat</h5>
                    <p class="card-text text-justify">Jalan Cihampelas No. 173 Bandung 40131</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" id="gmap_canvas" 
                src="https://maps.google.com/maps?q=sman%202%20bandung&t=&z=17&ie=UTF8&iwloc=&output=embed">
            </iframe>
        </div>
    </div>
</div>
@endsection