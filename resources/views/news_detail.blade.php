@extends('layouts.app')

@section('content')
<div class="container content content-navbar-p">
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="pt-4 row d-flex justify-content-center">
        <img class="img-fluid" src="{{--{{ $berita->img_url }}--}}{{ asset('/img/bulan_bahasa_WEB.png') }}">
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="pt-4 row d-flex justify-content-center">
        <div class="col-10">
            <h2 class="title-underline color-smanda font-weight-bold">{{--{{$berita->judul}}--}}Judul Berita</h2>
        </div>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <div class="col-10">
        <h5 class="color-secondary">{{--{{$berita->tanggal}}--}}27/15/2020</h5>
        </div>
    </div>
    <div class="row pt-4 d-flex justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-10">
            <p class="text-justify">{{--{{$berita->Konten}}--}}Kota Bandung adalah kota yang paling banyak memiliki berbagai julukan, seperti Paris van Java, Kota Bunga, 
                Kota Bandung Lautan Api, Kota Kuliner, Kota Wisata, Kota Pendidikan, Kota Kreatif, Kota Seniman, dll. Dengan 
                ketinggian 700 m di atas permuakaan laut, kota ini bermula dari tempat pelesiran bangsawan Belanda di jaman 
                penjajahan dulu. Dengan udaranya yang terbilang sejuk, kota ini menjadi dambaan banyak orang untuk mengeyam 
                pendidikan di kota Bandung. Sebagai contoh, ITB adalah salah satu icon pendidikan yang ada di Indonesia dan 
                beberapa perguruan tinggi terkenal lainnya.</p>
        </div>
    </div>
</div>