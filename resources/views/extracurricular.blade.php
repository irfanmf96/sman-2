@extends('layouts.app')

@section('content')
<div class="container content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <i class="fas fa-bell color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Ekstrakurikuler</h2>
    </div>
    <div class="row pt-4 justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-10">
            <p class="text-justify">Kurikulum Tingkat Satuan Pendidikan (KTSP) adalah kurikulum operasional yang disusun 
                oleh dan dilaksanakan di masing-masing satuan pendidikan yang berfungsi sebagai pedoman penyelenggaraan 
                kegiatan pembelajaran untuk mencapai tujuan pendidikan tertentu. Tujuan tertentu ini meliputi tujuan 
                pendidikan nasional serta kesesuaian dengan kekhasan, kondisi dan potensi daerah, satuan pendidikan dan 
                peserta didik. Oleh sebab itu, KTSP harus disusun sesuai dengan kebutuhan, karakteristik, dan potensi 
                satuan pendidikan (internal) serta lingkungan di daerah setempat.</p>
            <p class="text-justify">Kurikulum Tingkat Satuan Pendidikan (KTSP) adalah kurikulum operasional yang disusun 
                oleh dan dilaksanakan di masing-masing satuan pendidikan yang berfungsi sebagai pedoman penyelenggaraan 
                kegiatan pembelajaran untuk mencapai tujuan pendidikan tertentu. Tujuan tertentu ini meliputi tujuan 
                pendidikan nasional serta kesesuaian dengan kekhasan, kondisi dan potensi daerah, satuan pendidikan dan 
                peserta didik. Oleh sebab itu, KTSP harus disusun sesuai dengan kebutuhan, karakteristik, dan potensi 
                satuan pendidikan (internal) serta lingkungan di daerah setempat.</p>
            <p class="text-justify">Kegiatan pengembangan diri merupakan kegiatan pendidikan di luar mata pelajaran 
                sebagai bagian integral dari isi kurikulum sekolah. Kegiatan pengembangan diri merupakan upaya pembentukan 
                watak dan kepribadian peserta didik yang dilakukan melalui kegiatan layanan konseling dan kegiatan 
                ekstrakurikuler. Kegiatan ekstrakurikuler merupakan wadah yang disediakan oleh satuan pendidikan untuk 
                menyalurkan minat, bakat, hobi, kepribadian, dan kreativitas peserta didik yang dapat dijadikan sebagai 
                alat untuk mendeteksi talenta peserta didik.</p>
            <p class="text-justify">Berangkat dari uraian di atas, SMA Negeri 2 Bandung dengan dikoordinir oleh OSIS 
                menyelenggarakan berbagai kegiatan ekstrakurikuler di berbagai bidang, baik di bidang olahraga, pencapaian 
                akademik, kemampuan berbahasa, analisis pikir, kemampuan sosial, kesehatan, dan pengembaraan alam.</p>
        </div>
    </div>
    <div class="row pt-2 pb-4 d-flex justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-10">
            <h4 class="color-smanda title-underline"><b>Beberapa Ektrakurikuler yang terdapat di SMAN 2 Bandung</b></h4>
        </div>
    </div>
    <div class="row pt-2 pb-4 d-flex justify-content-center">
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Keluarga Remaja Masjid.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/pasukan Pengibar bendera.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Gerakan Pramuka.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Rumah Seni.png') }}"></a>
        </div>
    </div>
    <div class="row pt-2 pb-4 d-flex justify-content-center">
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Gerakan Pecinta Alam.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Taekwondo.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Softball.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Futsal.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Green Charets Community.png') }}"></a>
        </div>
    </div>
    <div class="row pt-2 pb-4 d-flex justify-content-center">
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Koperasi Siswa.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Palang Meraha Remaja.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Letter 2 Jurnalis.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/ekskul/Student IT Community.png') }}"></a>
        </div>
    </div>
</div>
@endsection