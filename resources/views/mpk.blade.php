@extends('layouts.app')

@section('content')
<!--MPK-->
<div class="container content content-navbar-p">
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <i class="fas fa-users color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">MPK (Musyawarah Perwakilan Kelas)</h2>
    </div>
    <div class="row pt-4 justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-md-10 col-lg-2 pb-3 text-center">
            <img class="img-fluid" src="{{ asset('img/sekbid/mpk.png') }}">
        </div>
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-md-10 col-lg-8">
            <p class="text-justify">MPK atau Musyawarah Perwakilan Kelas adalah sebuah lembaga resmi yang berkedudukan sejajar 
                dengan Ketua OSIS. Fungsi dari Perwakilan Kelas adalah sebagai aspirator yaitu penerima aspirasi dari seluruh 
                warga SMA Negeri 2 Bandung, evaluator yaitu pengevaluasi kinerja OSIS dalam satu periode, dan planner yaitu 
                perancang atau perencana, merencanakan Anggaran Dasar dan Anggaran Rumah Tangga (AD/ART) dan program kerja OSIS 
                selama satu periode.</p>
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-10">
            <h4 class="color-smanda title-underline"><b>Susunan Kepengurusan Periode LXII Masa Bakti 2014-2015</b></h4>
            <h5 class="color-smanda komisi-title title-non-underline">Komisi A</h5>
        </div>
    </div>
    <div class="row pt-3 d-flex">
        <div class="col-sm-9">
            <div class="offset-2 pl-1 btn-group dropleft">
                <button type="button" class="btn btn-smanda dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <b>Bagian</b>
                </button>
                <div class="dropdown-menu" style="cursor:pointer">
                    <a class="dropdown-item" onclick="changeTable(0)">Komisi A</a>
                    <a class="dropdown-item" onclick="changeTable(1)">Komisi B</a>
                    <a class="dropdown-item" onclick="changeTable(2)">Komisi C</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row pt-1 pb-5 d-flex justify-content-center">
        <div class="col-sm-9 col-md-9 hscroll">
            <table id="komisi-a" class="table table-striped table-sm table-responsive-sm">
                <thead class="table-smanda">
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Jabatan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Raihan Lutfianto</td>
                        <td>XI IPA 4</td>
                        <td>Ketua Umum</td>
                    </tr>
                    <tr>
                        <td>Muhammad Dezha Detiro</td>
                        <td>XI MIA 8</td>
                        <td>Wakil Ketua I</td>
                    </tr>
                    <tr>
                        <td>Yedida Emetetha</td>
                        <td>XI MIA 6</td>
                        <td>Wakil Ketua II</td>
                    </tr>
                    <tr>
                        <td>Devita Utami Mardiani</td>
                        <td>XI MIA 2</td>
                        <td>Sekretaris Umum</td>
                    </tr>
                    <tr>
                        <td>Kirana Salma Dewi</td>
                        <td>XI MIA 2</td>
                        <td>Sekretaris I</td>
                    </tr>
                    <tr>
                        <td>Sritasya Annisa Pramesti</td>
                        <td>X-A</td>
                        <td>Sekretaris II</td>
                    </tr>
                    <tr>
                        <td>Dinda Fadhila Sari</td>
                        <td>XI MIA 6</td>
                        <td>Bendahara Umum</td>
                    </tr>
                    <tr>
                        <td>Putri Indah Sari</td>
                        <td>XI MIA 8</td>
                        <td>Bendahara I</td>
                    </tr>
                    <tr>
                        <td>Astari Khairunnisa</td>
                        <td>X-G</td>
                        <td>Bendahara II</td>
                    </tr>
                </tbody>
            </table>
            <table id="komisi-b" class="table table-striped table-sm table-responsive-sm d-none">
                <thead class="table-smanda">
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Jabatan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Kismiazi</td>
                        <td>XI MIA 3</td>
                        <td>Koordinator Bidang I</td>
                    </tr>
                    <tr>
                        <td>Siti Hayati Zakiyah</td>
                        <td>XI IIS 1</td>
                        <td>Wakil Koordinator Bidang I</td>
                    </tr>
                    <tr>
                        <td>Aulia Lubbi Luthfan</td>
                        <td>X-D</td>
                        <td>Sie. Agama Islam</td>
                    </tr>
                    <tr>
                        <td>Truly Aulia Fadhlika</td>
                        <td>X-F</td>
                        <td>Sie. Agama Islam</td>
                    </tr>
                    <tr>
                        <td>Hagi Fauzan Firmansyah</td>
                        <td>X-I</td>
                        <td>Sie. Agama Kristen</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="{{ asset('js/animateCSS.js') }}"></script>
<script>
    function changeTable(komisi)
    {
        if(komisi==0)
        {
            $("#komisi-a").removeClass("d-none");
            $(".komisi-title").text("Komisi A");
            animateCSS("#komisi-a", "fadeIn");
            $("#komisi-b").addClass("d-none");
        }
        if(komisi==1)
        {
            $("#komisi-b").removeClass("d-none");
            $(".komisi-title").text("Komisi B");
            animateCSS("#komisi-b", "fadeIn");
            $("#komisi-a").addClass("d-none");
        }
    }
</script>
@endsection