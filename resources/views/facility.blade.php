@extends('layouts.app')

@section('content')
<div class="container content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="pt-4 row d-flex justify-content-center">
        <i class="fas fa-school color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Sarana dan Prasarana</h2>
    </div>
    <div class="row pt-4 d-flex justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-10">
            <p class="card-text text-justify">SMA Negeri 2 Bandung memiliki luas tanah kurang lebih seluas 2,1 hektar dan merupakan sekolah 
                terluas di kota Bandung. 60% dari luas tanah ini merupakan ruang terbuka hijau. Sarana dan prasarana yang tersedia di SMAN 
                2 Bandung sangat lengkap mulai dari fasilitas umum, fasilitas siswa, fasilitas guru, fasilitas olahraga yang mencangkup lapangan futsal, lapangan basket, 
                lapangan voli, dan trek lari. Juga terdapat Fasilitas penunjang Ekstrakulikuler seperti studi musik, lab komputer, lab fisika, 
                lab kimia, dan lain-lain.</p>
            <p class="card-text text-justify">Selain itu di SMAN 2 Juga terdapat <i>Green House</i> dimana didalamnya terdapat berbagai 
                macam tumbuhan dari berbagai jenis. Dan tidak lupa juga SMAN 2 mempunyai fasilitas Mesjid yang sangat besar dan sangat 
                nyaman yang dapat menampung kapasitas jamaah dengan jumlah yang besar.</p>
        </div>
    </div>
    <div class="row pt-4 d-flex justify-content-center">
        <h4 class="title-underline color-smanda font-weight-bold"></h4>
    </div>
</div>