@extends('layouts.app')

@section('content')
<div class="container content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <i class="fas fa-landmark color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Sejarah Singkat</h2>
    </div>
    <div class="row pt-4 d-flex justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-10">
            <p class="text-justify">Kota Bandung adalah kota yang paling banyak memiliki berbagai julukan, seperti Paris van Java, Kota Bunga, 
                Kota Bandung Lautan Api, Kota Kuliner, Kota Wisata, Kota Pendidikan, Kota Kreatif, Kota Seniman, dll. Dengan 
                ketinggian 700 m di atas permuakaan laut, kota ini bermula dari tempat pelesiran bangsawan Belanda di jaman 
                penjajahan dulu. Dengan udaranya yang terbilang sejuk, kota ini menjadi dambaan banyak orang untuk mengeyam 
                pendidikan di kota Bandung. Sebagai contoh, ITB adalah salah satu icon pendidikan yang ada di Indonesia dan 
                beberapa perguruan tinggi terkenal lainnya.</p>
            <p class="text-justify">SMA Negeri 2 Bandung berdiri dengan resmi tahun 1949 diprakarsai oleh Thio Anio sekaligus bertindak sebagai 
                Kepala Sekolah. Pada saat berdirinya SMAN 2 Bandung berlokasi di Jl. Kasatrian, di gedung SMPN 1 yang 
                lokasinya berdekatan dengan SD Douwes Decker. Tetapi hal ini hanya berlangsung beberapa bulan saja. Pada 
                tahun yang sama, SMAN 2 Bandung pindah ke Jl. Belitung No. 08, yang saat ini digunakan oleh SMAN 3 Bandung 
                dan SMAN 5. Pada awalnya SMAN 2 disebut SMA B yang merupakan bagian dari AMS sie B, atau eksakta yang 
                mengutamakan pelajaran Matematika dan Fisika. Pada tanggal 2 Agustus 1952 resmi berdiri SMAN 2 BANDUNG.</p>
            <p class="text-justify">Pada tahun 1966 terjadi pergolakan fisik yang hebat dan kampus sekolah Cina berada di Jalan Cihampelas pun 
                berhasil direbut oleh pejuang muda. Sejarah itulah yang mengawali berpindahnya SMA 2 dari Jalan Belitung ke 
                Jalan Cihampelas hingga sekarang.</p>
            <p class="text-justify">Dalam perjalanan SMA Negeri 2 Bandung hingga sekarang, beberapa kali dipercaya untuk membina persiapan 
                pembentukan SMA Negeri baru, diantaranya adalah SMA Negeri 3, 15 dan SMA Negeri 23 Bandung. Dalam kurun waktu 
                yang panjang itulah sekian banyak nama Kepala Sekolah yang memimpin SMAN 2 Bandung, yakni sebagai berikut :</p>
        </div>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true">
        <div class="row pt-4 d-flex justify-content-center text-center">
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/M_Entoem.png') }}" alt="" class="old-headmaster-pics">
                <a><br>M. Entoem<br>(1952-1965)</a>
            </div>
            <div class="col-2 ">
                <img src="{{ asset('/img/headmaster_history/Drs.-Ibnu-Hadi.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Drs. Ibnu Hadi<br>(1966-1974)</a>
            </div>
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/Drs.-Nana-Kusnadi.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Drs. Nana Kusnadi<br>(1966)</a>
            </div>
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/Drs.-Ahmad-hamid.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Drs. Ahmad Hamid<br>(1974-1982)</a>
            </div>
        </div>
        <div class="row pt-4 d-flex justify-content-center text-center">
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/Drs.-Dono-Yusuf.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Drs. Dono Yusuf<br>(1982-1987)</a>
            </div>
            <div class="col-2 ">
                <img src="{{ asset('/img/headmaster_history/Drs_E_Mulyadi.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Drs. E. Mulyadi<br>(1987-1990)</a>
            </div>
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/Drs_Ihot_Muslihat.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Drs. Ihot Muslihat<br>(1990-1994)</a>
            </div>
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/H_Ena_Sumpena.png') }}" alt="" class="old-headmaster-pics">
                <a><br>H. Ena Sumpena<br>(1994-1999)</a>
            </div>
        </div>
        <div class="row pb-4 pt-4 d-flex justify-content-center text-center">
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/Drs_H_Ruhaedi_W.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Drs. H. Ruhaedi W.<br>(1999-2003)</a>
            </div>
            <div class="col-2 ">
                <img src="{{ asset('/img/headmaster_history/Drs-H-Encang.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Drs H. Encang<br>(2004-2008)</a>
            </div>
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/H_Teddy_Hidayat_SPd.png') }}" alt="" class="old-headmaster-pics">
                <a><br>H. Teddy Hidayat S.Pd M.M.Pd<br>(2008-2013)</a>
            </div>
            <div class="col-2">
                <img src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" alt="" class="old-headmaster-pics">
                <a><br>Dr. Hj. Sundari, M.Pd.<br>(2013-2016)</a>
            </div>
        </div>
    </div>
</div>
@endsection