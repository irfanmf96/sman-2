@extends('layouts.app')

@section('content')
<div class="container content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <i class="fas fa-award color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Visi dan Misi</h2>
    </div>
    <div class="row pt-4 d-flex justify-content-center">
        <div class="animated flipInY slow col-sm-12 col-md-6 col-lg-5 pb-4">
            <div class="card vismis_card alt-pallate">
                <div class="card-body color-smanda-alt">
                    <h5 class="card-title font-weight-bold title-underline-alt">Visi</h5>
                    <p class="card-text"><b>Visi:</b> Mewujudkan Sekolah yang Unggul, religius, inovatif berbudaya lingkungan dan mampu bersaing
                    ditingkat nasional maupun internasional.</p><br>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-5 pb-4">
            <div class="animated flipInY slow card vismis_card alt-pallate">
                <div class="card-body color-smanda-alt">
                    <h5 class="card-title font-weight-bold title-underline-alt">Misi</h5>
                    <p class="card-text"><b>Misi:</b> Meningkatkan kecerdasan dan iklim edukatif pada diri Pendidik,Peserta Didik dan Tenaga 
                    Kependidikan melelui Pelatihan Workshop dan Lokakarya sehingga terbentuk pribadi-pribadi yang unggul 
                    dalam bidang akademik dan non akademik serta berwawasan lingkungan dan berperan aktif dalam pelestarian 
                    lingkungan serta mencegah pencemaran dan kerusakan lingkungan.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection