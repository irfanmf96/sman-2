@extends('layouts.app')

@section('content')
<div class="container-fluid content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <i class="fas fa-photo-video color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Galeri</h2>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row no-gutters py-4 d-flex justify-content-end">
        <div class="col-sm-4 col-md-3 image-box">
            <img id="1stImage" class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
        <div class="col-sm-4 col-md-3 image-box">
            <img class="img-fluid" src="{{ asset('img/ekskul/ants.png') }}"></a>
        </div>
    </div>
    <!-- The Modal -->
    <div id="myModal" class="modal">
        <span class="close">&times;</span>
        <img class="modal-content" src="" id="img01" alt="">
        <div id="caption"></div>
    </div>
</div>
<script>
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    $(".image-box").on("click", function(){
        $("#myModal").addClass("d-block");
        console.log( $("#1stImage").attr('src'));
        $("#img01").attr('src', $("#1stImage").attr('src'));
        $("#caption").text() = this.attr('alt');
    })

    // When the user clicks on <span> (x), close the modal
    $(".close").on("click", function() {
        $("#myModal").removeClass("d-block");
    })
</script>
@endsection