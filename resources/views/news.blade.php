@extends('layouts.app')
<link href="{{ asset('/css/news.css') }}" rel="stylesheet">
@section('content')
<div class="container content content-navbar-p">
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <i class="far fa-newspaper color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Berita</h2>
    </div>
    <div class="vgr-cards pt-3">
    @foreach($blog as $blog_)
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row card default-pallate">
            <div class="card-img-body">
                <img class="card-img" src="{{ $blog_->header_image }}" alt="Card image cap">
            </div>
            <div class="card-body p-4">
                <h4 class="card-title">{{$blog_->title}}</h4>
                <a class="card-text text-secondary">{{$blog_->created_at}}</a>
                <p class="card-text truncate">{{$blog_->content}}</p>
                <a href="route('news-detail'){{$blog_->id}}{{ route('news_detail') }}" class="btn news-card-button">Baca lebih lanjut</a>
            </div>
        </div>
    @endforeach
    </div>
    <div class="row pt-3 justify-content-end">
        {{$blog->links()}}
    </div>
</div>
@endsection