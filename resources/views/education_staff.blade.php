@extends('layouts.app')

@section('content')
<div class="container content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center pt-4">
        <i class="fas fa-pen-fancy color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Kepala Sekolah dan Komite</h2>
    </div>
    <div class="row py-5 d-flex justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-6 text-center">
            <h4 class="color-smanda font-weight-bold">Kepala Sekolah</h4>
            <a class="font-weight-bold">Drs. Deddy Chrisdiarto</a><br>
            <img class="img-fluid" src="{{ asset('/img/education_staff/drs_deddy_chrisdiarto.png') }}" alt=""><br><br>
            <div class="row d-flex justify-content-center">
                <div class="col-2 d-flex justify-content-end"><b>NIP:</b></div>
                <div class="col-6 d-flex justify-content-start text-left">19611224 198203 1 008</div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-2 d-flex justify-content-end"><b>TTL:</b></div>
                <div class="col-6 d-flex justify-content-start text-left">Tanjung Karang, 24/12/1961</div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-2 d-flex justify-content-end"><b>Umur:</b></div>
                <div class="col-6 d-flex justify-content-start text-left">54 tahun</div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-2 d-flex justify-content-end"><b>Jabatan:</b></div>
                <div class="col-6 d-flex justify-content-start text-left">Kepala SMA Negeri 2 Bandung</div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-2 d-flex justify-content-end"><b>Pangkat:</b></div>
                <div class="col-6 d-flex justify-content-start text-left">IV/A</div>
            </div>
        </div>
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-6 text-center">
            <h4 class="color-smanda font-weight-bold">Komite Sekolah</h4>
            <a class="font-weight-bold">Drs H. Dudun Abdullah SQ. MM</a><br>
            <img class="img-fluid" src="{{ asset('/img/education_staff/dudun_abdullah_blue.png') }}" alt=""><br><br>
            <div class="row d-flex justify-content-center">
                <div class="col-sm-9 col-md-7 hscroll">
                    <table class="table table-striped table-sm table-responsive-sm">
                        <thead class="table-smanda">
                            <tr>
                                <th scope="col">Anggota:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Ny. Hj. Tjutju Susilowati</td>
                            </tr>
                            <tr>
                                <td>H. Munaji R. Hidayat, SE, M.M</td>
                            </tr>
                            <tr>
                                <td>Reni Susanti</td>
                            </tr>
                            <tr>
                                <td>Dr. H. Tatang Mulyana, M.Pd</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center pt-4">
        <i class="fas fa-sitemap color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Struktur Organisasi</h2>
    </div>
    <div class="row pt-3 d-flex justify-content-center">
        <div class="col-9"><img class="img-fluid" src="{{ asset('/img/organigram.png') }}" alt=""></div>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center pt-5">
        <i class="fas fa-chalkboard-teacher color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Daftar Guru dan Karyawan Tata Usaha</h2>
    </div>
    <div class="row pt-4 pb-5 d-flex justify-content-center">
        <div class="col-md-10 col-sm-12 hscroll">
            <table class="table DataTables table-striped" style="width:100%">
                <thead class="table-smanda">
                    <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Pelajaran</th>
                        <th>Tugas</th>
                        <th>Kelamin</th>
                    </tr>
                </thead>
                <tbody>
                <!-- Hapus {{-- dan --}} serta data dummy saat implimentasi -->
                {{--@foreach($gurus as $guru)--}}
                    <tr>
                        <td>{{--{{$guru->NIP}}--}} 100000008</td>
                        <td>{{--{{$guru->Nama}}--}} Kusnadi, S.Pdt</td>
                        <td>{{--{{$guru->Mapel}}--}} Bahasa Sunda</td>
                        <td>{{--{{$guru->Jabatan}}--}} Guru</td>
                        <td>{{--{{$guru->Jeniskelamin}}--}} L</td>
                    </tr>
                {{--@endforeach--}}
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection