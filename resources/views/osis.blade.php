@extends('layouts.app')

@section('content')
<div class="container content content-navbar-p" >
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row d-flex justify-content-center">
        <i class="fas fa-users color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">OSIS (Organisasi Siswa Intra Sekolah)</h2>
    </div>
    <div class="row pt-4 justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-md-10 col-lg-2 pb-3 text-center">
            <img src="{{ asset('img/sekbid/osis.png') }}">
        </div>
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-md-10 col-lg-8">
            <p class="text-justify">OSIS (Organisasi Siswa Intra Sekolah) di SMA Negeri 2 Bandung merupakan satu dari dua organisasi 
                kesiswaan resmi di SMA Negeri 2 Bandung, selain MPK. Organisasi ini diurus dan dikelola oleh para siswa yang terpilih 
                menjadi pengurus OSIS. Di SMA Negeri 2 Bandung, OSIS dibimbing oleh Majelis Bimbingan Organisasi (MBO), yang dipimpin 
                langsung oleh Kepala SMA Negeri 2 Bandung bersama Wakil Kepala Sekolah Bidang Kesiswaan.TB adalah salah satu icon 
                pendidikan yang ada di Indonesia dan beberapa perguruan tinggi terkenal lainnya.</p>
            <p class="text-justify">Seluruh siswa SMA Negeri 2 Bandung memiliki hak untuk menjadi pengurus OSIS dengan mengikuti tahapan 
                seleksi yang disebut Pemilihan OSIS Dua (POD). Selanjutnya, para pengurus OSIS tersebut melaksanakan program kerja 
                yang telah disusun, dengan memperhatikan usulan dan arahan dari MBO serta Musyawarah Perwakilan Kelas.</p>
        </div>
    </div>
    <div class="row pt-2 pb-4 d-flex justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-10">
            <h4 class="color-smanda title-underline"><b>Sekertaris Bidang OSIS</b></h4>
        </div>
    </div>
    <div class="row pt-2 pb-4 d-flex justify-content-center">
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid d-block" src="{{ asset('img/sekbid/sekbid-1.png') }}"></a>
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid" src="{{ asset('img/sekbid/sekbid-2.png') }}">
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid" src="{{ asset('img/sekbid/sekbid-3.png') }}">
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid" src="{{ asset('img/sekbid/sekbid-4.png') }}">
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid" src="{{ asset('img/sekbid/sekbid-5.png') }}">
        </div>
    </div>
    <div class="row pt-2 pb-4 d-flex justify-content-center">
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid" src="{{ asset('img/sekbid/sekbid-6.png') }}">
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid" src="{{ asset('img/sekbid/sekbid-7.png') }}">
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid" src="{{ asset('img/sekbid/sekbid-8.png') }}">
        </div>
        <div class="col-md-2 col-sm-3">
            <img class="img-fluid" src="{{ asset('img/sekbid/sekbid-9.png') }}">
        </div>
    </div>
    <div class="row d-flex justify-content-center">
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-10">
            <h4 class="color-smanda title-underline"><b>Susunan Kepengurusan Periode LXII Masa Bakti 2014-2015</b></h4>
            <h5 class="color-smanda sekbid-title title-non-underline">Pengurus Utama</h5>
        </div>
    </div>
    <div class="row pt-3 d-flex">
        <div class="col-sm-9">
            <div class="offset-2 pl-1 btn-group dropleft">
                <button type="button" class="btn btn-smanda dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <b>Bagian</b>
                </button>
                <div class="dropdown-menu" style="cursor:pointer">
                    <a class="dropdown-item" onclick="changeTable(0)">Pengurus Utama</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" onclick="changeTable(1)">Sekbid 1</a>
                    <a class="dropdown-item" onclick="changeTable(2)">Sekbid 2</a>
                    <a class="dropdown-item" onclick="changeTable(3)">Sekbid 3</a>
                    <a class="dropdown-item" onclick="changeTable(4)">Sekbid 4</a>
                    <a class="dropdown-item" onclick="changeTable(5)">Sekbid 5</a>
                    <a class="dropdown-item" onclick="changeTable(6)">Sekbid 6</a>
                    <a class="dropdown-item" onclick="changeTable(7)">Sekbid 7</a>
                    <a class="dropdown-item" onclick="changeTable(8)">Sekbid 8</a>
                    <a class="dropdown-item" onclick="changeTable(9)">Sekbid 9</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row pt-1 pb-5 d-flex justify-content-center">
        <div class="col-sm-9 col-md-9 hscroll">
            <table id="sekbid-0" class="table table-striped table-sm table-responsive-sm">
                <thead class="table-smanda">
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Jabatan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Raihan Lutfianto</td>
                        <td>XI IPA 4</td>
                        <td>Ketua Umum</td>
                    </tr>
                    <tr>
                        <td>Muhammad Dezha Detiro</td>
                        <td>XI MIA 8</td>
                        <td>Wakil Ketua I</td>
                    </tr>
                    <tr>
                        <td>Yedida Emetetha</td>
                        <td>XI MIA 6</td>
                        <td>Wakil Ketua II</td>
                    </tr>
                    <tr>
                        <td>Devita Utami Mardiani</td>
                        <td>XI MIA 2</td>
                        <td>Sekretaris Umum</td>
                    </tr>
                    <tr>
                        <td>Kirana Salma Dewi</td>
                        <td>XI MIA 2</td>
                        <td>Sekretaris I</td>
                    </tr>
                    <tr>
                        <td>Sritasya Annisa Pramesti</td>
                        <td>X-A</td>
                        <td>Sekretaris II</td>
                    </tr>
                    <tr>
                        <td>Dinda Fadhila Sari</td>
                        <td>XI MIA 6</td>
                        <td>Bendahara Umum</td>
                    </tr>
                    <tr>
                        <td>Putri Indah Sari</td>
                        <td>XI MIA 8</td>
                        <td>Bendahara I</td>
                    </tr>
                    <tr>
                        <td>Astari Khairunnisa</td>
                        <td>X-G</td>
                        <td>Bendahara II</td>
                    </tr>
                </tbody>
            </table>
            <table id="sekbid-1" class="table table-striped table-sm table-responsive-sm d-none">
                <thead class="table-smanda">
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Jabatan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Kismiazi</td>
                        <td>XI MIA 3</td>
                        <td>Koordinator Bidang I</td>
                    </tr>
                    <tr>
                        <td>Siti Hayati Zakiyah</td>
                        <td>XI IIS 1</td>
                        <td>Wakil Koordinator Bidang I</td>
                    </tr>
                    <tr>
                        <td>Aulia Lubbi Luthfan</td>
                        <td>X-D</td>
                        <td>Sie. Agama Islam</td>
                    </tr>
                    <tr>
                        <td>Truly Aulia Fadhlika</td>
                        <td>X-F</td>
                        <td>Sie. Agama Islam</td>
                    </tr>
                    <tr>
                        <td>Hagi Fauzan Firmansyah</td>
                        <td>X-I</td>
                        <td>Sie. Agama Kristen</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="{{ asset('js/animateCSS.js') }}"></script>
<script>
    function changeTable(sekbid)
    {
        if(sekbid==0)
        {
            $("#sekbid-0").removeClass("d-none");
            $(".sekbid-title").text("Pengurus Utama");
            animateCSS("#sekbid-0", "fadeIn");
            $("#sekbid-1").addClass("d-none animated fadeIn");
        }
        if(sekbid==1)
        {
            $("#sekbid-1").removeClass("d-none");
            $(".sekbid-title").text("Sekertaris Bidang I: Pembinaan Keimanan dan Ketaqwaan Terhadap Tuhan Yang Maha Esa dan Pembinaan Ahklak Mulia");
            animateCSS("#sekbid-1", "fadeIn");
            $("#sekbid-0").addClass("d-none");
        }
        if(part==10)
        {
            $("#komisi-a").removeClass("d-none");
            $(".komisi-title").text("Komisi A");
            animateCSS("#komisi-a", "fadeIn");
            $("#komisi-b").addClass("d-none");
        }
        if(part==11)
        {
            $("#komisi-b").removeClass("d-none");
            $(".komisi-title").text("Komisi B");
            animateCSS("#komisi-b", "fadeIn");
            $("#komisi-a").addClass("d-none");
        }
    }
</script>
@endsection