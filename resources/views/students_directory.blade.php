@extends('layouts.app')

@section('content')
<div class="container-fluid content content-navbar-alt-p">
    <div class="row d-flex justify-content-center">
        <h2 class="title-underline color-smanda font-weight-bold">Direktori Guru dan Karyawan Tata Usaha</h2>
    </div>
    <div class="row pt-5 d-flex justify-content-center">
        <div class="col-10">
            <table id="example" class="table display" style="width:100%">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col" width="40%">Status</th>
                        <th scope="col" width="40%">Keterangan</th>
                        <th scope="col" class="text-center" width="20%">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <p><b>test</b></p>
                        </td>
                        <td>
                            <p><b>test</b></p>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><b>test</b></p>
                        </td>
                        <td>
                            <p><b>test</b></p>
                        </td>
                        <td>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js_page')
<script>
    $('#example').DataTable( {
		"paging":   false,
		"ordering": false,
		"info":    false
	} );
</script>