@extends('layouts.app')

@section('content')
<div class="container-fluid content">
    <div class="row animated pulse slower">
        <header id="banner">
        </header>
    </div>
    <div class="row pt-5 d-flex justify-content-center">
        <i class="far fa-newspaper color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold">Berita Terbaru</h2>
    </div>
    <div class="row pt-3">
        @foreach($blog as $blog_)
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-lg-4 mb-4">
            <div class="card news-card-main">
                <img height="375px" width="375px" src="{{$blog_->header_image}}" alt="" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">{{$blog_->title}}</h5>
                    <a class="card-text text-secondary">{{$blog_->created_at}}</a>
                    <p class="card-text text-justify">{{$blog_->content}}</p>
                    <a href="" class="btn news-card-button btn-sm">Baca lebih lanjut</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="pt-4 row d-flex justify-content-center alt-pallate">
        <i data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="fas fa-school color-smanda-alt fa-2x pr-1"></i>
        <h2 data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="title-underline-alt color-smanda-alt font-weight-bold">Seputar Tentang SMAN 2 Bandung</h2>
    </div>
    <div class="row alt-pallate">
        <div class="col-lg-2 col-sm-3"></div>
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-lg-4 col-sm-6">
            <img class="py-3 img-fluid" src="{{ asset('/img/Tentang_Smanda.jpg') }}">
        </div>
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-lg-5 col-sm-12">
            <p class="text-justify pt-3 color-smanda-alt">SMA Negeri 2 Bandung merupakan salah satu sekolah menengah atas berwawasan 
                lingkungan dan berstandar nasional yang berada di Kota Bandung, Jawa Barat, Indonesia. Sekolah ini berlokasi di 
                Jalan Cihampelas nomor 173, Kelurahan Cipaganti, Kecamatan Coblong, Kota Bandung. Masa pendidikan di SMA Negeri 
                2 Bandung ditempuh dalam waktu tiga tahun pelajaran, mulai dari kelas X hingga kelas XII, seperti pada umumnya 
                masa pendidikan menengah atas di Indonesia.</p>
            <p class="text-justify color-smanda-alt">SMA Negeri 2 Bandung didirikan pada tahun 1949 oleh Thio Anio. 
                Pada 2 Agustus 1952, SMA Negeri 2 Bandung resmi berdiri, hasil pemekaran dan reorganisasi sekolah yang terletak di 
                Jalan Belitung. Pada tahun 2007, sekolah ini menggunakan Kurikulum Tingkat Satuan Pendidikan, setelah sebelumnya 
                menggunakan Kurikulum Berbasis Kompetensi.</p>
        </div>
    </div>
    <div class="row pb-5 alt-pallate">
        <div class="col-lg-2"></div>
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-sm-6 col-md-5 col-lg-3 pt-3">
            <div class="card about-card">
                <div class="card-body color-smanda">
                    <h5 class="card-title font-weight-bold title-underline">Visi dan Misi</h5>
                    <p class="card-text text-justify"><b>Visi:</b> Mewujudkan Sekolah yang Unggul, religius, inovatif berbudaya lingkungan dan mampu bersaing
                    ditingkat nasional maupun internasional.</p>
                    <p class="card-text text-justify"><b>Misi:</b> Meningkatkan kecerdasan dan iklim edukatif pada diri Pendidik,Peserta Didik dan Tenaga 
                    Kependidikan melelui Pelatihan Workshop dan Lokakarya sehingga terbentuk pribadi-pribadi yang unggul 
                    dalam bidang akademik dan non akademik serta berwawasan lingkungan dan berperan aktif dalam pelestarian 
                    lingkungan serta mencegah pencemaran dan kerusakan lingkungan.</p>
                </div>
            </div>
        </div>
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-sm-6 col-md-6 col-lg-3 pt-3">
            <div class="card about-card">
                <div class="card-body color-smanda">
                    <h5 class="card-title font-weight-bold title-underline">Prestasi Sekolah</h5>
                    <p class="card-text text-justify">Sekolah SMAN 2 Bandung sudah mendapatkan banyak piala penghargaan 
                    mulai dari tingkat nasional maupun internasional di berbagai bidang seperti Futsal, Basket, Cerdas 
                    cermat, penghargaan sekolah bersih, dan masih banyak lainnya. 
                    
                    <p class="card-text text-justify">Salah satu kebanggaan kami yaitu pada Delegasi SMAN 2 Bandung 
                    menjuarai Lomba Cerdas Cermat Empat Pilar Kehidupan Berbangsa dan Bernegara tingkat Kota Bandung dan 
                    mewakili Kota Bandung dalam LCC tingkat Provinsi Jawa Barat yang dilaksanakan MPR-RI. Selain itu masih 
                    banyak penghargaan yang sudah pernah dirain oleh SMAN 2 Bandung.</p>
                </div>
            </div>
        </div>
        <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="col-sm-6 col-md-6 col-lg-3 pt-3">
            <div class="card about-card">
                <div class="card-body color-smanda">
                    <h5 class="card-title font-weight-bold title-underline">Sarana dan Prasana</h5>
                    <p class="card-text text-justify">Sarana dan prasarana yang tersedia di SMAN 2 Bandung sangat lengkap mulai dari 
                    fasilitas umum, fasilitas siswa, fasilitas guru, fasilitas olahraga yang mencangkup lapangan futsal, lapangan basket, 
                    lapangan voli, dan trek lari. Juga terdapat Fasilitas penunjang Ekstrakulikuler seperti studi musik, lab komputer, lab fisika, 
                    lab kimia, dan lain-lain.</p>
                    <p class="card-text text-justify">Selain itu di SMAN 2 Juga terdapat <i>Green House</i> dimana didalamnya terdapat berbagai 
                    macam tumbuhan dari berbagai jenis. Dan tidak lupa juga SMAN 2 mempunyai fasilitas Mesjid yang sangat besar dan sangat 
                    nyaman yang dapat menampung kapasitas jamaah dengan jumlah yang besar.</p>
                </div>
            </div>
        </div>
    </div>
    <div data-aos="fade-in" data-aos-duration="2000" data-aos-once="true" class="row pt-3 d-flex justify-content-center">
        <i class="fas fa-map-marker-alt color-smanda fa-2x pr-1"></i><h2 class="title-underline color-smanda font-weight-bold ">Lokasi</h2>
    </div>
    <div class="row d-flex justify-content-center pt-4">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" id="gmap_canvas" 
                src="https://maps.google.com/maps?q=sman%202%20bandung&t=&z=17&ie=UTF8&iwloc=&output=embed">
            </iframe>
        </div>
    </div>
</div>
@endsection
