<!--NOTE! Dikarenakan desain website menggabungkan desain tipe one page dan multi page, maka akan ada 2 "app.blade.php"
yang digunakan, halaman ini dan "app.blade.php"-->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SMAN 2 BANDUNG</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="panda eduProject X-Project ">
  <meta content="" name="design page for Laravel v.1">

  <!-- Favicons -->
  <link href="{{ asset('img/favicon.png') }}" rel="icon">
  <link href="{{ asset('img/panda.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet">
  <link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" rel="stylesheet">
  <link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('lib/animate/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">
  <link href="{{ asset('lib/fontawesome/css/all.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">

 </head>

  <body>

    <!--==========================
      Header
    ============================-->
    <header id="header">
      <div class="container-fluid">

        <div id="logo" class="pull-left">
          <!-- Uncomment below if you prefer to use an image logo -->
          <h1><a href="{{route('home')}}"><img class="" width="40px" height="40px" src="{{ asset('img/logo_sman2_bandung_2.png')}}" alt="X-Project" title="X-Project"/> SMAN 2 BANDUNG</a></h1>
        </div>

        <nav id="nav-menu-container">
          <ul class="nav-menu">
            <!--<li class="menu-active"><a href="#intro">Beranda</a></li>-->
            <li><a href="#latest-news">Berita</a></li>
            <li><a href="#about">Tentang SMAN 2</a></li>
            <li><a href="#facts">Tenaga Pendidik</a></li>
            <li><a href="#team">Kesiswaan</a></li>
            <li><a href="#team">Kontak dan Alamat</a></li>
            <li><a href="{{ route('betaGallery') }}">Galeri</a></li>
            <!--<li class="menu-has-children"><a href="">Admin</a>
              <ul>
                <li><a href="http://10.10.11.10/eduOnepage" target="_blank">eduOnePage</a></li>
                <li><a href="#">Charets Admin Panel</a></li>
                <li><a href="#">DFD Center</a></li>
                <li><a href="#">ERD Center</a></li>
                <li><a href="http://10.10.11.10:3445/admin" target="_blank">eduProject - Charets</a></li>
                <li><a href="#">e-commerce Model</a></li>
                <li><a href="#">SIMDIK</a></li>
                <li><a href="http://10.10.11.10:8787/" target="_blank">Charets Shop</a></li>
              </ul>
            </li>-->
            <li><a href="" data-toggle="modal" data-target=".loginModal">Login</a></li>
          </ul>
        </nav><!-- #nav-menu-container -->
      </div>
    </header><!-- #header -->

    <!--==========================
      Login Section
    ============================-->

    <div class="modal fade loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-login">
        <div class="modal-content">
          <div class="modal-header">				
            <h4 class="modal-title">Login</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <form action="{{route('login-admin')}}" method="post">
              @csrf
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="email" class="form-control" name="email" placeholder="Email" required="required">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                  <input type="password" class="form-control" name="password" placeholder="Password" required="required">
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-lg">Login</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>     

    <!--==========================
      Intro Section - Carousel Concept with Bread on Data On Admin Panel. liat source code panda#211
    ============================-->
    <section id="intro">
      <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

          <ol class="carousel-indicators"></ol>

          <div class="carousel-inner" role="listbox">

            <div class="carousel-item active">
              <div class="carousel-background"><img src="img/header/header2.jpg" alt=""></div>
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2>Selamat Datang di SMAN 2 Bandung</h2>
                  <p>
                    SMAN 2 Bandung merupakan salah satu SMA terbaik di Indonesia, dengan lulusan yang berhasil diterima
                    universitas ternama di Indonesia seperti ITB, UNPAD, UI, UGM, dan lainnya.
                  </p>
                  <!--<a href="#featured-services" class="btn-get-started scrollto">Yuk simak..</a>-->
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="carousel-background"><img src="img/header/pohon_karet.jpg" alt=""></div>
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2>Peraih Penghargaan Sekolah Hijau se-Indonesia</h2>
                  <p>Dikarenakan banyaknya tumbuhan, luas, dan nyamannya lingkungan SMAN 2 Bandung. SMAN 2 Bandung berhasil memenangkan
                    penghargaan sekolah hijau di Indonesia.
                  </p>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="carousel-background"><img src="img/header/lapangan.jpg" alt=""></div>
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2>Sekolah Negeri Terluas di Kota Bandung</h2>
                  <p>Dengan luas tanah kurang lebih seluas 2,1 hektar, 60% dari luas tanah ini merupakan ruang terbuka hijau.
                    SMAN 2 Bandung ini tidak hanya nyaman tapi juga dilengkapi oleh banyak fasilitas seperti laboratorium sains,
                    ruang ekskul, 2 lapangan olahraga, track lari, gor, dan masih banyak lainnya. 
                  </p>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="carousel-background"><img src="img/header/prestasi.jpg" alt=""></div>
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2>Penghargaan Siswa!</h2>
                  <p>Insert penghargaan siswa here.</p>
                </div>
              </div>
            </div>

          </div>

          <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>

          <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>

        </div>
      </div>
    </section><!-- #intro -->

    <main id="main">

      <!--==========================
        Featured Services Section
      ============================-->
      <!--<section id="featured-services">
        <div class="container">
          <div class="row">

            <div class="col-lg-4 box">
              <i class="ion-ios-bookmarks-outline"></i>
              <h4 class="title"><a href="">Analisis Requrement System</a></h4>
              <p class="description">Perancangan sistem, dari mimpi sampe ke kemungkinan teknis modifikasi dan update coding, termasuk potensi celah keamanan</p>
            </div>

            <div class="col-lg-4 box box-bg">
              <i class="ion-ios-stopwatch-outline"></i>
              <h4 class="title"><a href="">Schema Proses dan Data</a></h4>
              <p class="description">Pemetaan dan Gambaran DFD, ERD dan semua yang berkaitan dengan bussiness Process dari hasil Analisis Requrement</p>
            </div>

            <div class="col-lg-4 box">
              <i class="ion-ios-heart-outline"></i>
              <h4 class="title"><a href="">Proses Development</a></h4>
              <p class="description">Development sistem harus berprinsip clean Code, Traceable, terintegrasi dan memberi jalan untuk Sync dengan system atau perangkat lain.</p>
            </div>

          </div>
        </div>
      </section>--><!-- #featured-services -->

      <!--==========================
        latest-news Section
      ============================-->
      <section id="latest-news"  class="section-bg" >
        <div class="container">

          <header class="section-header">
            <h3 class="section-title">Berita Terbaru</h3>
          </header>
          @foreach($blog as $index)
          <div class="row latest-news-container">

            <div class="col-lg-4 col-md-6 latest-news-item filter-app wow fadeInUp">
              <div class="latest-news-wrap">
                <figure>
                  <img src="{{$index->header_image}}" class="img-fluid" alt="">
                  <a href="{{$index->header_image}}" data-lightbox="latest-news" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="latest-news-info">
                  <h4><a href="#">{{$index->title}}</a></h4>
                  <p>{{$index->created_at}}</p>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          <div class="row">
            <div class="col-12 d-flex justify-content-end"><a class="smanda-btn" href="{{ route('betaNews') }}">Lihat Semua Berita</a></div>
          </div>
        </div>
      </section><!-- #latest-news -->

      <!--==========================
        About Us Section
      ============================-->
      <section id="about">
        <div class="container">

          <header class="section-header">
            <h3>Seputar Tentang SMAN 2 BANDUNG</h3>
            <p>SMA Negeri 2 Bandung merupakan salah satu sekolah menengah atas 
              berwawasan lingkungan dan berstandar nasional yang berada di Kota 
              Bandung, Jawa Barat, Indonesia. SMA Negeri 2 Bandung memiliki luas 
              tanah kurang lebih seluas 2,1 hektar dan merupakan sekolah terluas di 
              kota Bandung. 60% dari luas tanah ini merupakan ruang terbuka hijau.
              Sekolah SMAN 2 Bandung sudah mendapatkan banyak piala penghargaan 
              mulai dari tingkat nasional maupun internasional di berbagai bidang 
              seperti Futsal, Basket, Cerdas cermat , penghargaan sekolah bersih 
              dan masih banyak lainnya.
            </p>
          </header>

          <div class="row about-cols">

            <div class="col-md-4 wow fadeInUp">
              <div class="about-col">
                <div class="img">
                  <img src="img/seputar/Misi.jpg" alt="" class="img-fluid">
                  <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                </div>
                <h2 class="title"><a href="#">Misi</a></h2>
                <p>
                  Meningkatkan kecerdasan dan iklim edukatif pada diri Pendidik, 
                  Peserta Didik dan Tenaga Kependidikan melelui Pelatihan Workshop 
                  dan Lokakarya sehingga terbentuk pribadi-pribadi yang unggul 
                  dalam bidang akademik dan non akademik serta berwawasan 
                  lingkungan dan berperan aktif dalam pelestarian lingkungan serta 
                  mencegah pencemaran dan kerusakan lingkungan.
                </p>
              </div>
            </div>

            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
              <div class="about-col">
                <div class="img">
                  <img src="img/seputar/visi.jpg" alt="" class="img-fluid">
                  <div class="icon"><i class="ion-ios-eye-outline"></i></div>
                </div>
                <h2 class="title"><a href="#">Visi</a></h2>
                <p>
                  Mewujudkan Sekolah yang Unggul, religius, inovatif berbudaya lingkungan dan mampu bersaing ditingkat 
                  nasional maupun internasional.
                </p>
              </div>
            </div>

            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
              <div class="about-col">
                <div class="img">
                  <img src="img/seputar/Sejarah.jpg" alt="" class="img-fluid">
                  <div class="icon"><i class="fas fa-globe-americas"></i></div>
                </div>
                <h2 class="title"><a href="#">Sejarah</a></h2>
                <p>
                SMA Negeri 2 Bandung didirikan pada tahun 1949 oleh Thio Anio. Pada 2 Agustus 1952, SMA Negeri 2 Bandung 
                resmi berdiri, hasil pemekaran dan reorganisasi sekolah yang terletak di Jalan Belitung. Pada tahun 2007, 
                sekolah ini menggunakan Kurikulum Tingkat Satuan Pendidikan, setelah sebelumnya menggunakan Kurikulum 
                Berbasis Kompetensi.
                </p>
                <div class="pb-3 d-flex justify-content-center">
                  <button class="text-center" data-toggle="modal" data-target=".sejarah-modal-lg">Informasi lebih lanjut</button> 
                </div>
              </div>
            </div>

          </div>

        </div>
      </section><!-- #about -->

      <!--==========================
        Sejarah Modal Section
      ============================-->

      <div class="modal fade sejarah-modal-lg" tabindex="-1" role="dialog" aria-labelledby="sejarahModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title color-smanda text-uppercase" id="sejarahModal"><b>Sejarah SMAN 2 Bandung</b></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row px-3">
                <p>
                Kota Bandung adalah kota yang paling banyak memiliki berbagai julukan, seperti Paris van Java, Kota Bunga, Kota Bandung Lautan Api, Kota Kuliner, Kota Wisata, Kota Pendidikan, Kota Kreatif, Kota Seniman, dll. Dengan ketinggian 700 m di atas permuakaan laut, kota ini bermula dari tempat pelesiran bangsawan Belanda di jaman penjajahan dulu. Dengan udaranya yang terbilang sejuk, kota ini menjadi dambaan banyak orang untuk mengeyam pendidikan di kota Bandung. Sebagai contoh, ITB adalah salah satu icon pendidikan yang ada di Indonesia dan beberapa perguruan tinggi terkenal lainnya.
                </p>
                <p>
                SMA Negeri 2 Bandung berdiri dengan resmi tahun 1949 diprakarsai oleh Thio Anio sekaligus bertindak sebagai Kepala Sekolah. Pada saat berdirinya SMAN 2 Bandung berlokasi di Jl. Kasatrian, di gedung SMPN 1 yang lokasinya berdekatan dengan SD Douwes Decker. Tetapi hal ini hanya berlangsung beberapa bulan saja. Pada tahun yang sama, SMAN 2 Bandung pindah ke Jl. Belitung No. 08, yang saat ini digunakan oleh SMAN 3 Bandung dan SMAN 5. Pada awalnya SMAN 2 disebut SMA B yang merupakan bagian dari AMS sie B, atau eksakta yang mengutamakan pelajaran Matematika dan Fisika. Pada tanggal 2 Agustus 1952 resmi berdiri SMAN 2 BANDUNG.
                </p>
                <p>
                Pada tahun 1966 terjadi pergolakan fisik yang hebat dan kampus sekolah Cina berada di Jalan Cihampelas pun berhasil direbut oleh pejuang muda. Sejarah itulah yang mengawali berpindahnya SMA 2 dari Jalan Belitung ke Jalan Cihampelas hingga sekarang.
                </p>
                <p>
                Dalam perjalanan SMA Negeri 2 Bandung hingga sekarang, beberapa kali dipercaya untuk membina persiapan pembentukan SMA Negeri baru, diantaranya adalah SMA Negeri 3, 15 dan SMA Negeri 23 Bandung. Dalam kurun waktu yang panjang itulah sekian banyak nama Kepala Sekolah yang memimpin SMAN 2 Bandung, yakni sebagai berikut: 
                </p>
              </div>
              <div class="row">
                <div class="col-md-3 col-sm-4 text-center">
                  <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" style="border-radius: 50%;" alt="Hello">
                  <p class="text-center">Drs. Deddy Chrisdiarto<br>(1952-1965)</p>
                </div>
                <div class="col-md-3 col-sm-4 text-center">
                  <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" style="border-radius: 50%;" alt="Hello">
                  <p class="text-center">Drs. Deddy Chrisdiarto<br>(1952-1965)</p>
                </div>
                <div class="col-md-3 col-sm-4 text-center">
                  <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" style="border-radius: 50%;" alt="Hello">
                  <p class="text-center">Drs. Deddy Chrisdiarto<br>(1952-1965)</p>
                </div>
                <div class="col-md-3 col-sm-4 text-center">
                  <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" style="border-radius: 50%;" alt="Hello">
                  <p class="text-center">Drs. Deddy Chrisdiarto<br>(1952-1965)</p>
                </div>
                <div class="col-md-3 col-sm-4 text-center">
                  <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" style="border-radius: 50%;" alt="Hello">
                  <p class="text-center">Drs. Deddy Chrisdiarto<br>(1952-1965)</p>
                </div>
                <div class="col-md-3 col-sm-4 text-center">
                  <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" style="border-radius: 50%;" alt="Hello">
                  <p class="text-center">Drs. Deddy Chrisdiarto<br>(1952-1965)</p>
                </div>
                <div class="col-md-3 col-sm-4 text-center">
                  <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" style="border-radius: 50%;" alt="Hello">
                  <p class="text-center">Drs. Deddy Chrisdiarto<br>(1952-1965)</p>
                </div>
                <div class="col-md-3 col-sm-4 text-center">
                  <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" style="border-radius: 50%;" alt="Hello">
                  <p class="text-center">Drs. Deddy Chrisdiarto<br>(1952-1965)</p>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <!--==========================
        Headmaster Opening Section
      ============================-->
      <section id="headmaster-op">
        <div class="container">

          <header class="section-header">
            <h3>Kata Sambutan Kepala Sekolah</h3>
            <p><b>Drs. Deddy Chrisdiarto</b></p>
          </header>

          <div class="row">
            <div class="col-sm-12 col-md-3 d-flex justify-content-center">
              <img src="{{ asset('img/education_staff/drs_deddy_chrisdiarto.png') }}" alt="" class="img-fluid">
            </div>
            <div class="col-sm-12 col-md-9">
              <p>SMA Negeri 2 Bandung merupakan salah satu sekolah menengah atas 
                berwawasan lingkungan dan berstandar nasional yang berada di Kota 
                Bandung, Jawa Barat, Indonesia. SMA Negeri 2 Bandung memiliki luas 
                tanah kurang lebih seluas 2,1 hektar dan merupakan sekolah terluas di 
                kota Bandung. 60% dari luas tanah ini merupakan ruang terbuka hijau.
                Sekolah SMAN 2 Bandung sudah mendapatkan banyak piala penghargaan 
                mulai dari tingkat nasional maupun internasional di berbagai bidang 
                seperti Futsal, Basket, Cerdas cermat , penghargaan sekolah bersih 
                dan masih banyak lainnya.
              </p>
            </div>
          </div>
        </div>
      </section>

      <!--==========================
        Environment Section
      ============================-->
      <section id="environment">
        <div class="container">
          <header class="section-header">
            <h3>Sarana dan Prasarana</h3>
          </header>
          <div class="row">
            <div class="col-12 env-carousel">
              <div><img src="{{ asset('img/sarana_prasarana/1.png') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/2.png') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/3.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/4.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/5.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/6.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/7.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/8.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/9.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/10.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/11.jpg') }}" alt="" class="img-fluid"></div>
              <div><img src="{{ asset('img/sarana_prasarana/12.jpg') }}" alt="" class="img-fluid"></div>
            </div>
          </div>
        </div>
      </section>

      <!--==========================
        Facts-staff Section
      ============================-->
      <section id="facts-staff"  class="wow fadeIn">
        <div class="container">

          <header class="section-header">
            <h3>Tenaga Pendidik</h3>
            <p>
              SMAN 2 Bandung memiliki banyak tenaga pendidik yang membantu berjalannya
              proses pendidikan sehari-hari.
            </p>
          </header>

          <div class="row counters">

            <div class="col-lg-4 col-6 text-center">
              <span data-toggle="counter-up">120</span>
              <p>Jumlah Guru Pengajar</p>
            </div>

            <div class="col-lg-4 col-6 text-center">
              <span data-toggle="counter-up">70</span>
              <p>Jumlah Staff TU</p>
            </div>

            <div class="col-lg-4 col-12 text-center">
              <span data-toggle="counter-up">100</span>
              <p>Jumlah Tenaga Pendidik lainnya</p>
            </div>
          </div>
          
          <div class="row pb-5">
              <div class="col-12 d-flex justify-content-center"><a class="smanda-btn" href="#">Organigram</a></div>
              <div class="col-12 d-flex justify-content-center"><a class="smanda-btn" href="" data-toggle="modal" data-target=".staff-modal-lg">Direktori Tenaga Pendidik</a></div>
          </div>
        </div>
      </section>

       <!--==========================
        Directory Edu Staff Modal Section
      ============================-->

      <div class="modal fade staff-modal-lg" tabindex="-1" role="dialog" aria-labelledby="staffModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title color-smanda text-uppercase" id="staffModal"><b>Direktori Tenaga Pendidik SMAN 2 Bandung</b></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row px-3 d-flex justify-content-center">
                <div class="col-12 hscroll">
                  <table class="table DataTables table-striped" style="width:100%">
                    <thead class="table-smanda">
                      <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Pelajaran</th>
                        <th>Tugas</th>
                        <th>Kelamin</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>100000008</td>
                        <td>Kusnadi, S.Pdt</td>
                        <td>Bahasa Sunda</td>
                        <td>Guru</td>
                        <td>L</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <!--==========================
        Facts-students Section
      ============================-->
      <section id="facts-students"  class="wow fadeIn">
        <div class="container">

          <header class="section-header">
            <h3>Siswa dan Alumni</h3>
            <p>SMAN 2 Bandung memiliki banyak murid yang pintar-pintar dan alumni yang berhasil diterima di 
              universitas-universitas ternama.</p>
          </header>

          <div class="row pb-5 counters">

            <div class="col-6 text-center">
              <span data-toggle="counter-up">1400</span>
              <p>Jumlah Siswa Aktif</p>
              <a class="smanda-btn" href="#" data-toggle="modal" data-target=".students-modal-lg">Direktori Siswa Aktif</a>
            </div>

            <div class="col-6 text-center">
              <span data-toggle="counter-up">50000</span>
              <p>Jumlah Alumni</p>
              <a class="smanda-btn" href="#" data-toggle="modal" data-target=".alumni-modal-lg">Direktori Alumni</a>
            </div>
          </div>
        </div>
      </section>

      <!--==========================
        Directory Students Modal Section
      ============================-->

      <div class="modal fade students-modal-lg" tabindex="-1" role="dialog" aria-labelledby="studentsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title color-smanda text-uppercase" id="studentsModal"><b>Direktori Siswa Aktif SMAN 2 Bandung</b></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row px-3 d-flex justify-content-center">
                <div class="col-12 hscroll">
                  <table class="table DataTables table-striped" style="width:100%">
                    <thead class="table-smanda">
                      <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Pelajaran</th>
                        <th>Tugas</th>
                        <th>Kelamin</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>100000008</td>
                        <td>Kusnadi, S.Pdt</td>
                        <td>Bahasa Sunda</td>
                        <td>Guru</td>
                        <td>L</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <!--==========================
        Directory Alumni Modal Section
      ============================-->

      <div class="modal fade alumni-modal-lg" tabindex="-1" role="dialog" aria-labelledby="alumniModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title color-smanda text-uppercase" id="alumniModal"><b>Direktori Alumni SMAN 2 Bandung</b></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row px-3 d-flex justify-content-center">
                <div class="col-12 hscroll-lg">
                  <table class="table DataTables table-striped" style="width:100%">
                    <thead class="table-smanda">
                      <tr>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Pelajaran</th>
                        <th>Tugas</th>
                        <th>Kelamin</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>100000008</td>
                        <td>Kusnadi, S.Pdt</td>
                        <td>Bahasa Sunda</td>
                        <td>Guru</td>
                        <td>L</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <!--==========================
        University Alumni Section
      ============================-->
      <section id="univ-alumni">
        <div class="container">

          <header class="section-header">
            <h3>Persebaran Universitas Alumni SMAN 2 Bandung</h3>
            <p>Universitas-universitas yang diraih oleh lulusan SMAN 2 Bandung.</p>
          </header>

          <div class="skills-content">

            <div class="progress">
              <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
                <span class="skill">Universitas Teknologi Bandung<i class="val">2000 Alumni</i></span>
              </div>
            </div>

            <div class="progress">
              <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                <span class="skill">Universitas Padjajaran <i class="val">1800 Alumni</i></span>
              </div>
            </div>

            <div class="progress">
              <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
                <span class="skill">Universitas Katolik Parahyangan<i class="val">1400 Alumni</i></span>
              </div>
            </div>

            <div class="progress">
              <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                <span class="skill">Universitas Telkom<i class="val">1000 Alumni</i></span>
              </div>
            </div>

          </div>

        </div>
      </section>

      <!--==========================
        Organization Section
      ============================-->
      <section id="environment">
        <div class="container">
          <header class="section-header">
            <h3>Organisasi dan Ekstrakurikuler Siswa</h3>
            <p>SMAN 2 Bandung memiliki banyak Ekstrakurikuler dan Organisasi 
              Siswa yang bisa membantu dalam mengembangkan <i>soft skill</i> siswa.</p>
          </header>
          <div class="row pb-2">
            <div class="col-6 d-flex justify-content-end">
              <a href="" data-toggle="modal" data-target=".org-modal-lg"><img src="img/ekskul/mpk.png"  alt="" class="img-fluid img-org"></a>
            </div>
            <div class="col-6">
              <img src="img/ekskul/osis.png" alt="" class="img-fluid img-org">
            </div>
          </div>
          <div class="row pb-2">
            <div class="col-2">
              <img src="img/ekskul/Futsal.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/ants.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/softball.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Taekwondo.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Student%20IT%20Community.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Gerakan%20Pecinta%20Alam.png" alt="" class="img-fluid img-org">
            </div>
          </div>
          <div class="row pb-2">
            <div class="col-2">
              <img src="img/ekskul/Gerakan%20Pramuka.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Green%20Charets%20Community.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Keluarga%20Remaja%20Masjid.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Koperasi%20Siswa.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Letter%202%20Jurnalis.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Palang%20Meraha%20Remaja.png" alt="" class="img-fluid img-org">
            </div>
          </div>
          <div class="row pb-2">
            <div class="col-2">
              <img src="img/ekskul/Pasukan%20Pengibar%20Bendera.png" alt="" class="img-fluid img-org">
            </div>
            <div class="col-2">
              <img src="img/ekskul/Rumah%20Seni.png" alt="" class="img-fluid img-org">
            </div>
          </div>
        </div>
      </section>

      <!--==========================
        Organization Modal Section
      ============================-->

      <div class="modal fade org-modal-lg" tabindex="-1" role="dialog" aria-labelledby="orgModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title color-smanda text-uppercase" id="orgModal"><b>Insert Extrakurikuler name here</b></h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row pb-3 d-flex justify-content-center">
                <img class="img-fluid" src="{{ asset('/img/headmaster_history/Dr_Hj_Sundari_MPd.png') }}" alt="Hello">
              </div>
              <div class="row px-3 text-center">
                <p>SMA Negeri 2 Bandung merupakan salah satu sekolah menengah atas berwawasan lingkungan dan berstandar nasional yang berada di Kota Bandung, Jawa Barat, Indonesia. SMA Negeri 2 Bandung memiliki luas tanah kurang lebih seluas 2,1 hektar dan merupakan sekolah terluas di kota Bandung. 60% dari luas tanah ini merupakan ruang terbuka hijau. Sekolah SMAN 2 Bandung sudah mendapatkan banyak piala penghargaan mulai dari tingkat nasional maupun internasional di berbagai bidang seperti Futsal, Basket, Cerdas cermat , penghargaan sekolah bersih dan masih banyak lainnya.</p>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <!--==========================
        Services Section
      ============================-->
      <!--<section id="services">
        <div class="container">

          <header class="section-header wow fadeInUp">
            <h3>Ruang Lingkup</h3>
            <p>-------------------------------------------------------------------------------------</p>
          </header>

          <div class="row">

            <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
              <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
              <h4 class="title"><a href="">RDBMS</a></h4>
              <p class="description">hayooo.. yang baru gabung, tau gak apa itu RDBMS? bukan merek Velg ya.. cek dulu d mbah google gih..</p>
            </div>
            <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
              <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
              <h4 class="title"><a href="">Bootstrap Templating</a></h4>
              <p class="description">untung udah ada bootstrap ya.. klo enggak, kebayang harus bikin style dari awal ampe akhir.. </p>
            </div>
            <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
              <div class="icon"><i class="ion-ios-paper-outline"></i></div>
              <h4 class="title"><a href="">Framework Base Development</a></h4>
              <p class="description">Framework apapun pasti lebih rapi dari pada ngoOding sendiri. </p>
            </div>
            <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
              <h4 class="title"><a href="">Recruitment Admin</a></h4>
              <p class="description">biar gk L10. kecuali dikantor loe emang gak da siapa-siapa lagi selain tamu sama pengunjung.</p>
            </div>
            <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="icon"><i class="ion-ios-barcode-outline"></i></div>
              <h4 class="title"><a href="">Stressing System</a></h4>
              <p class="description">Klo punya nyali, stressing system dong.. biar kliatan sebaper apa system yang dibikin</p>
            </div>
            <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
              <div class="icon"><i class="ion-ios-people-outline"></i></div>
              <h4 class="title"><a href="">Update</a></h4>
              <p class="description">Klo ternyata sistem nya baperan, ya wajib update berarti.. </p>
            </div>

          </div>

        </div>
      </section>--><!-- #services -->

      <!--==========================
        Call To Action Section
      ============================-->
      <!--<section id="call-to-action" class="wow fadeIn">
        <div class="container text-center">
          <h3>Mau Curhat?</h3>
          <p>Punya angan-angan atau mimpi trus pengen di raih tapi bingung harus mulai dari mana? yuk ngobrol... siapin kopi, indomie triple cheesseee, sama bigbang lechy..</p>
          <a class="cta-btn" href="#">Call To Action</a>
        </div>
      </section>--><!-- #call-to-action -->

  <!-- 
  =============================================================
  ini harusnya autoscreenschoot per jam terakhir alias si robot kpu tea
  =============================================================

          <div class="facts-img">
            <img src="img/facts-img.png" alt="" class="img-fluid">
          </div>
  -->

        </div>
      </section><!-- #facts -->

      <!--==========================
        Clients Section
      ============================-->
      <!--<section id="clients" class="wow fadeInUp">
        <div class="container">

          <header class="section-header">
            <h3>Obos-obos</h3>
          </header>

          <div class="owl-carousel clients-carousel">
            <img src="img/clients/client-1.png" alt="">
            <img src="img/clients/client-2.png" alt="">
            <img src="img/clients/client-3.png" alt="">
            <img src="img/clients/client-4.png" alt="">
            <img src="img/clients/client-5.png" alt="">
            <img src="img/clients/client-6.png" alt="">
            <img src="img/clients/client-7.png" alt="">
            <img src="img/clients/client-8.png" alt="">
          </div>

        </div>
      </section>--><!-- #clients -->

      <!--==========================
        Clients Section
      ============================-->
      <!--<section id="testimonials" class="section-bg wow fadeInUp">
        <div class="container">

          <header class="section-header">
            <h3>Testimonials</h3>
          </header>

          <div class="owl-carousel testimonials-carousel">

            <div class="testimonial-item">
              <img src="img/testimonial-1.jpg" class="testimonial-img" alt="">
              <h3>Drs.Utomo Prasetyo, M.Si.</h3>
              <h4>Coor. Tatib Charets</h4>
              <p>
                <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                Kalau menurut sayah mah keren banget lah pastinya, da karunya tara aya nu muji.. peupeuriheun tara pupujieun, ya saya puji we lah..
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
            </div>

            <div class="testimonial-item">
              <img src="img/testimonial-2.jpg" class="testimonial-img" alt="">
              <h3>H. Hamba Alloh, S.T.</h3>
              <h4>Design Graphic</h4>
              <p>
                <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                Bagi saya ini luar biasa semangatnya, karna bagi saya bagus pertama itu adalah semangatnya apalagi keikhlasannya, karena saya juga suka merasakan klo ngedesain cuma sebatas design.
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
            </div>

            <div class="testimonial-item">
              <img src="img/testimonial-3.jpg" class="testimonial-img" alt="">
              <h3>Ayuning Tias, S.Pd.</h3>
              <h4>Mantan Admin</h4>
              <p>
                <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                Tapi bener lho klo sebagai admin kerasa banget ini asyik. bikin betah ngoprek data karena dengan di buatkan system nya kayak gini jadi gampang dapet data dan informasi bekal ngambil tindakan dan diputusin.
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
            </div>

            <div class="testimonial-item">
              <img src="img/testimonial-4.jpg" class="testimonial-img" alt="">
              <h3>Panda</h3>
              <h4>CEO &amp; Founder eduProject</h4>
              <p>
                <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                Bagi saya mudah-mudah bagus dan bermanfaat bagi semua, sehingga jadi amalan yang mengalir sampai akhirat.. tapi kalaupun kurang bagus atau gatot kacau sekalipun kan setidaknya sudah berusaha, sudah mikir sudah mencoba.. mudah-mudahan tetap jadi ladang pahala.. Aaaaammmiiiin...
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
            </div>

            <div class="testimonial-item">
              <img src="img/testimonial-5.jpg" class="testimonial-img" alt="">
              <h3>Stakeholders</h3>
              <h4>Praktisi Pendidikan</h4>
              <p>
                <img src="img/quote-sign-left.png" class="quote-sign-left" alt="">
                Tentu ini sangat bermanfaat dan membantu manajemen dalam mengambil sebuah kebijakan. karena pada prinsip nya kebijakan itu harus diambil berdasarkan data dan informasi faktual yang update. Nah dengan system ini data dan informasi tersebut bisa jauh lebih cepat didapat.
                <img src="img/quote-sign-right.png" class="quote-sign-right" alt="">
              </p>
            </div>

          </div>

        </div>
      </section>--><!-- #testimonials -->

      <!--==========================
        Team Section
      ============================-->
      <!--<section id="team">
        <div class="container">
          <div class="section-header wow fadeInUp">
            <h3>Team</h3>
            <p>Untuk sementara masih Jomblo, maksudnya Single, Single Fighter karna ku Selow sungguh sellow santai.. santai... nanti yang ngelamar datang sendiri lah.. tuk yang mau ngelamar, Tukang desain vector/ilustrator/animator, Tukang Framework, Tukang RDBMS, Tukang Native Apps Android/iOS sok lah, kita ngobrol aja..</p>
          </div>

          <div class="row">

            <div class="col-lg-3 col-md-6 wow fadeInUp">
              <div class="member">
                <img src="img/team-1.jpg" class="img-fluid" alt="">
                <div class="member-info">
                  <div class="member-info-content">
                    <h4>Endra Suhada</h4>
                    <span>Chief Executive Officer</span>
                    <div class="social">
                      <a href=""><i class="fa fa-twitter"></i></a>
                      <a href=""><i class="fa fa-facebook"></i></a>
                      <a href=""><i class="fa fa-google-plus"></i></a>
                      <a href=""><i class="fa fa-linkedin"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
              <div class="member">
                <img src="img/team-2.jpg" class="img-fluid" alt="">
                <div class="member-info">
                  <div class="member-info-content">
                    <h4>Endra Suhada</h4>
                    <span>CEO &amp; Founder</span>
                    <div class="social">
                      <a href=""><i class="fa fa-twitter"></i></a>
                      <a href=""><i class="fa fa-facebook"></i></a>
                      <a href=""><i class="fa fa-google-plus"></i></a>
                      <a href=""><i class="fa fa-linkedin"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
              <div class="member">
                <img src="img/team-3.jpg" class="img-fluid" alt="">
                <div class="member-info">
                  <div class="member-info-content">
                    <h4>Endra Suhada</h4>
                    <span>Tukang RDBM &amp; Coding &amp; Gambar</span>
                    <div class="social">
                      <a href=""><i class="fa fa-twitter"></i></a>
                      <a href=""><i class="fa fa-facebook"></i></a>
                      <a href=""><i class="fa fa-google-plus"></i></a>
                      <a href=""><i class="fa fa-linkedin"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
              <div class="member">
                <img src="img/team-4.jpg" class="img-fluid" alt="">
                <div class="member-info">
                  <div class="member-info-content">
                    <h4>Endra Suhada</h4>
                    <span>Tukang Ngahuleng</span>
                    <span>Requrement Scope</span>
                    <div class="social">
                      <a href=""><i class="fa fa-twitter"></i></a>
                      <a href=""><i class="fa fa-facebook"></i></a>
                      <a href=""><i class="fa fa-google-plus"></i></a>
                      <a href=""><i class="fa fa-linkedin"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </section>--><!-- #team -->

      <!--==========================
        Contact Section
      ============================-->
      <section id="contact" class="section-bg wow fadeInUp">
        <div class="container">

          <div class="section-header">
            <h3>Kontak dan Alamat</h3>
          </div>

          <div class="row contact-info">
            <div class="col-md-4">
              <div class="contact-address">
                <i class="ion-ios-location-outline"></i>
                <h3>Alamat</h3>
                <address>Jalan Cihampelas No. 173 Bandung 40131</address>
              </div>
            </div>

            <div class="col-md-4">
              <div class="contact-phone">
                <i class="ion-ios-telephone-outline"></i>
                <h3>Telepon dan Fax</h3>
                <p>+62-22-2032462</p>
              </div>
            </div>

            <div class="col-md-4">
              <div class="contact-email">
                <i class="ion-ios-email-outline"></i>
                <h3>Email</h3>
                <p>Contact@sman2bdg.sch.id</p>
              </div>
            </div>

          </div>

          <!--<div class="form">
            <div id="sendmessage">Pesan Anda telah terkirim. Terimakasih.</div>
            <div id="errormessage"></div>
            <form action="mailto:abdadi87.droid@gmail.com" method="post" role="form" class="contactForm">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                <div class="form-group col-md-6">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validation"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>-->
        </div>
      </section><!-- #contact -->
       <!--==========================
        Location Section
      ============================-->

      <section id="location" class="wow fadeInUp">
        <div class="container-fluid">
          <div class="section-header">
            <h3>Lokasi</h3>
          </div>
          <div class="row d-flex justify-content-center">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" id="gmap_canvas" 
                    src="https://maps.google.com/maps?q=sman%202%20bandung&t=&z=17&ie=UTF8&iwloc=&output=embed">
                </iframe>
            </div>
          </div>
        </div>
      </section>
    </main>

    <!--==========================
      Footer
    ============================-->
    <footer id="footer">
      <!--<div class="footer-top">
        <div class="container">
          <div class="row">

            <div class="col-lg-3 col-md-6 footer-info">
              <h3>SMAN 2 Bandung</h3>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
              <h4>Jump Link</h4>
              <ul>
                <li><i class="ion-ios-arrow-right"></i> <a href="#">Home</a></li>
                <li><i class="ion-ios-arrow-right"></i> <a href="#">Sekapur Sirih</a></li>
                <li><i class="ion-ios-arrow-right"></i> <a href="#">Ruang Lingkup</a></li>
                <li><i class="ion-ios-arrow-right"></i> <a href="#">Prototype</a></li>
                <li><i class="ion-ios-arrow-right"></i> <a href="#">Update</a></li>
              </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-contact">
              <h4>Basis WorkStation</h4>
              <p>
                Cibaraja 10 8/3 <br>
                Cisaat, Kb.SMi<br>
                Jabar-INA <br>
                <strong>Telepon  :</strong> +62 813 8604 0087<br>
                <strong>Email    :</strong> abdadi87.droid@gmail.com<br>
              </p>

              <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              </div>

            </div>

            <div class="col-lg-3 col-md-6 footer-newsletter">
              <h4>Subscribe Newsletter</h4>
              <p>Silakan Subscribe untuk mendapatkan pemberitahuan dari setiap update X-Project. Nantikan kehadiran kami dalam edisi tutorial berbasis vlog.</p>
              <form action="" method="post">
                <input type="email" name="email"><input type="submit"  value="Subscribe">
              </form>
            </div>

          </div>
        </div>
      </div>-->

      <div class="container">
        <div class="pt-3 social-links text-center">
          <a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
          <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
          <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
          <a href="#" class="google-plus"><i class="fab fa-google-plus"></i></a>
          <a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
        </div>
        <div class="copyright">
          <strong>SMAN 2 Bandung </strong>&copy; <strong>
           2020
          </strong>
        </div>
        <div class="credits">        
          by: CV. Wangun Teknologi Bersama            
        </div>
      </div>
    </footer><!-- #footer -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <!-- Uncomment below i you want to use a preloader -->
    <!-- <div id="preloader"></div> -->

    <!-- JavaScript Libraries -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
    <script src="{{ asset('lib/superfish/hoverIntent.js') }}"></script>
    <script src="{{ asset('lib/superfish/superfish.min.js') }}"></script>
    <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
    <script src="{{ asset('lib/waypoints/waypoints.min.js') }}"></script>
    <script src="{{ asset('lib/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('lib/isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('lib/lightbox/js/lightbox.min.js') }}"></script>
    <script src="{{ asset('lib/touchSwipe/jquery.touchSwipe.min.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js" defer></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" defer></script>
    <!-- Contact Form JavaScript File -->
    <script src="contactform/contactform.js"></script>

    <!-- Template Main Javascript File -->
    <script src="{{ asset('js/main.js') }}"></script>

    <script type="text/javascript">
      $(document).ready(function(){
        $('.DataTables').DataTable();
        $('.env-carousel').slick({
          dots: true,
          infinite: true,
          autoplay: true,
          arrows: true,
          speed: 300,
          slidesToShow: 4,
          slidesToScroll: 4,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
        })
      })
    </script>

  </body>
</html>
