<!--==========================
  Portfolio Section
============================-->
@extends('BizPage.layouts.app')
<link href="{{ asset('/css/news.css') }}" rel="stylesheet">
@section('content')
<section id="news">
  <div class="container">
    <header class="section-header">
      <h3 class="section-title">Berita</h3>
    </header>
    <div class="vgr-cards pt-3">
      @foreach($blog as $index)
      <div class="row card default-pallate wow fadeInUp">
        <div class="card-img-body">
          <img class="card-img" src="{{$index->header_image}}" alt="Card image cap">
        </div>
        <div class="card-body p-4">
          <h4 class="card-title">{{$index->title}}</h4>
          <a class="card-text pb-3 text-secondary">{{$index->created_at}}</a>
          <p class="card-text truncate">{{$index->content}}!</p>
          <a href="" class="btn news-card-button">Baca lebih lanjut</a>
        </div>
      </div>
      @endforeach
      {{$blog->links()}}
    </div>
  </div>
</section>
@endsection