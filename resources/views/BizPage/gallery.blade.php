<!--==========================
Environment Section
============================-->
@extends('BizPage.layouts.app')
@section('content')
<section id="environment">
    <div class="container">
        <header class="section-header">
            <h3 class="section-title">Galeri</h3>
        </header>
        <div class="row">
            <h5><b><u>BAKSOS</u></b></h5>
            <div class="col-12 env-carousel">
                <div><img src="{{ asset('img/sarana_prasarana/1.png') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/2.png') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/3.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/4.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/5.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/6.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/7.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/8.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/9.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/10.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/11.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/12.jpg') }}" alt="" class="img-fluid"></div>
            </div>
        </div>
        <div class="row">
            <h5><b><u>BAKSOS</u></b></h5>
            <div class="col-12 env-carousel">
                <div><img src="{{ asset('img/sarana_prasarana/1.png') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/2.png') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/3.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/4.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/5.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/6.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/7.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/8.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/9.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/10.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/11.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/12.jpg') }}" alt="" class="img-fluid"></div>
            </div>
        </div>
        <div class="row">
            <h5><b><u>BAKSOS</u></b></h5>
            <div class="col-12 env-carousel">
                <div><img src="{{ asset('img/sarana_prasarana/1.png') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/2.png') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/3.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/4.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/5.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/6.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/7.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/8.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/9.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/10.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/11.jpg') }}" alt="" class="img-fluid"></div>
                <div><img src="{{ asset('img/sarana_prasarana/12.jpg') }}" alt="" class="img-fluid"></div>
            </div>
        </div>
    </div>
</section>
@endsection