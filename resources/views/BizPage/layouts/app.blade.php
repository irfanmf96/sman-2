<!--NOTE! Dikarenakan desain website menggabungkan desain tipe one page dan multi page, maka akan ada 2 "app.blade.php"
yang digunakan, halaman ini dan "index.blade.php"-->
<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8">
    <title>SMAN 2 BANDUNG</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="panda eduProject X-Project ">
    <meta content="" name="design page for Laravel v.1">

    <!-- Favicons -->
    <link href="{{ asset('img/favicon.png') }}" rel="icon">
    <link href="{{ asset('img/panda.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" rel="stylesheet">
    <link href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" rel="stylesheet">
    <link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/animate/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/fontawesome/css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

    <!-- Main Stylesheet File -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    </head>
    <body>

<!--==========================
  Header
============================-->
        <header id="header2" class="header-scrolled">
            <div class="container-fluid">

                <div id="logo" class="pull-left">
                <!-- Uncomment below if you prefer to use an image logo -->
                <h1><a href="#intro"><img class="" width="40px" height="40px" src="{{ asset('img/logo_sman2_bandung_2.png')}}" alt="X-Project" title="X-Project"/> SMAN 2 BANDUNG</a></h1>
                </div>

                <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <!--<li class="menu-active"><a href="#intro">Beranda</a></li>-->
                    <li><a href="{{ route('betaNews') }}">Berita</a></li>
                    <li><a href="#about">Tentang SMAN 2</a></li>
                    <li><a href="#facts">Tenaga Pendidik</a></li>
                    <li><a href="#team">Kesiswaan</a></li>
                    <li><a href="#team">Kontak dan Alamat</a></li>
                    <li><a href="#team">Galeri</a></li>
                    <!--<li class="menu-has-children"><a href="">Admin</a>
                    <ul>
                        <li><a href="http://10.10.11.10/eduOnepage" target="_blank">eduOnePage</a></li>
                        <li><a href="#">Charets Admin Panel</a></li>
                        <li><a href="#">DFD Center</a></li>
                        <li><a href="#">ERD Center</a></li>
                        <li><a href="http://10.10.11.10:3445/admin" target="_blank">eduProject - Charets</a></li>
                        <li><a href="#">e-commerce Model</a></li>
                        <li><a href="#">SIMDIK</a></li>
                        <li><a href="http://10.10.11.10:8787/" target="_blank">Charets Shop</a></li>
                    </ul>
                    </li>-->
                    <li><a href="#contact">Login</a></li>
                </ul>
                </nav><!-- #nav-menu-container -->
            </div>
        </header><!-- #header -->
        <main class="pt-4">
            @yield('content')
        </main>
        <footer id="footer">
            <!--<div class="footer-top">
                <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-info">
                    <h3>SMAN 2 Bandung</h3>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Jump Link</h4>
                    <ul>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Home</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Sekapur Sirih</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Ruang Lingkup</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Prototype</a></li>
                        <li><i class="ion-ios-arrow-right"></i> <a href="#">Update</a></li>
                    </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>Basis WorkStation</h4>
                    <p>
                        Cibaraja 10 8/3 <br>
                        Cisaat, Kb.SMi<br>
                        Jabar-INA <br>
                        <strong>Telepon  :</strong> +62 813 8604 0087<br>
                        <strong>Email    :</strong> abdadi87.droid@gmail.com<br>
                    </p>

                    <div class="social-links">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>

                    </div>

                    <div class="col-lg-3 col-md-6 footer-newsletter">
                    <h4>Subscribe Newsletter</h4>
                    <p>Silakan Subscribe untuk mendapatkan pemberitahuan dari setiap update X-Project. Nantikan kehadiran kami dalam edisi tutorial berbasis vlog.</p>
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit"  value="Subscribe">
                    </form>
                    </div>

                </div>
                </div>
            </div>-->

            <div class="container">
                <div class="pt-3 social-links text-center">
                <a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
                <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fab fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
                </div>
                <div class="copyright">
                <strong>SMAN 2 Bandung </strong>&copy; <strong>
                2020
                </strong>
                </div>
                <div class="credits">        
                by: CV. Wangun Teknologi Bersama            
                </div>
            </div>
        </footer><!-- #footer -->
        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

        <!-- JavaScript Libraries -->
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
        <script src="{{ asset('lib/superfish/hoverIntent.js') }}"></script>
        <script src="{{ asset('lib/superfish/superfish.min.js') }}"></script>
        <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
        <script src="{{ asset('lib/waypoints/waypoints.min.js') }}"></script>
        <script src="{{ asset('lib/counterup/counterup.min.js') }}"></script>
        <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('lib/isotope/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('lib/lightbox/js/lightbox.min.js') }}"></script>
        <script src="{{ asset('lib/touchSwipe/jquery.touchSwipe.min.js') }}"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js" defer></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" defer></script>
        <!-- Contact Form JavaScript File -->
        <script src="contactform/contactform.js"></script>

        <!-- Template Main Javascript File -->
        <script src="{{ asset('js/main.js') }}"></script>

        <script type="text/javascript">
        $(document).ready(function(){
            $('.DataTables').DataTable();
            $('.env-carousel').slick({
            dots: true,
            infinite: true,
            autoplay: true,
            arrows: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
            })
        })
        </script>
    </body>
</html>