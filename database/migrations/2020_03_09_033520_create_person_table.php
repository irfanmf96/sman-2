<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('pin');
            $table->string('address');
            $table->string('birth_place');
            $table->date('birth_date');
            $table->string('mobile_phone');
            $table->string('email');
            $table->string('pas_foto')->nullable();
            $table->smallInteger('active')->default(1);
            $table->foreignId('tenant_id')->references('id')->on('tenant');
            $table->foreignId('role_id')->references('id')->on('role');
            $table->foreignId('religion_id')->references('id')->on('religion');
            $table->foreignId('sex_id')->references('id')->on('sex');
            $table->foreignId('position_id')->nullable()->references('id')->on('positions');
            $table->foreignId('user_id')->nullable()->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person');
    }
}
