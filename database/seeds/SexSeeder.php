<?php

use App\Sex;
use Illuminate\Database\Seeder;

class SexTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Sex::class)->create(['sex_name' => 'Laki-Laki']);
        factory(Sex::class)->create(['sex_name' => 'Perempuan']);
    }
}
