<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Role::class)->create(['role_name' => 'Kepala Sekolah']);
        factory(Role::class)->create(['role_name' => 'Guru']);
        factory(Role::class)->create(['role_name' => 'Staff']);
        factory(Role::class)->create(['role_name' => 'Murid']);
    }
}
