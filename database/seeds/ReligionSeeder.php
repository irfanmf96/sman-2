<?php

use App\Religion;
use Illuminate\Database\Seeder;

class ReligionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Religion::class)->create(['religion_name' => 'Islam']);
        factory(Religion::class)->create(['religion_name' => 'Katolik']);
        factory(Religion::class)->create(['religion_name' => 'Protestan']);
        factory(Religion::class)->create(['religion_name' => 'Hindu']);
        factory(Religion::class)->create(['religion_name' => 'Budha']);
        factory(Religion::class)->create(['religion_name' => 'Konghucu']);
    }
}
