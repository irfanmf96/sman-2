<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ReligionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(SexTableSeeder::class);
        $this->call(TenantTableSeeder::class);
        $this->call(SemesterTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PositionTableSeeder::class);
        $this->call(PersonSeeder::class);
        
    }
}
