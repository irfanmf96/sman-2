<?php
use App\Person;

use Illuminate\Database\Seeder;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Person::class)->create(['first_name' => 'Irfan', 'last_name' => 'Muhammad Fauzi', 'pin' => '24071996', 'mobile_phone'=> '081312484824','address' => 'Jln. Muararajeun Lama no.8 Bandung', 'birth_place' => 'Bandung', 'birth_date' => '1996-07-24', 'email' => 'sman2admin@gmail.com', 'tenant_id' => 1, 'role_id' => 3, 'religion_id' => 1, 'sex_id' => 1 , 'user_id' => 1, 'position_id' => 2]);
    }
}
