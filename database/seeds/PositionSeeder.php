<?php

use App\Position;
use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Position::class)->create(['position_name' => 'Kepala Sekolah']);
        factory(Position::class)->create(['position_name' => 'Komite Sekolah']);
        factory(Position::class)->create(['position_name' => 'Kepala Tata Usaha']);
        factory(Position::class)->create(['position_name' => 'Wakasek Kurikulum']);
        factory(Position::class)->create(['position_name' => 'Wakasek Sarana']);
        factory(Position::class)->create(['position_name' => 'Wakasek Kesiswaan']);
        factory(Position::class)->create(['position_name' => 'Wakasek Humas']);
        factory(Position::class)->create(['position_name' => 'Tim Manajemen Mutu']);
        factory(Position::class)->create(['position_name' => 'Bid. Program Akademik']);
        factory(Position::class)->create(['position_name' => 'Bid. Pembelajaran']);
        factory(Position::class)->create(['position_name' => 'Bid. Pengolahan Data']);
        factory(Position::class)->create(['position_name' => 'Bid. Fasilitas Pendidikan']);
        factory(Position::class)->create(['position_name' => 'Bid. K3P4LH RTH']);
        factory(Position::class)->create(['position_name' => 'Bid. Inventaris']);
        factory(Position::class)->create(['position_name' => 'Pembina OSIS']);
        factory(Position::class)->create(['position_name' => 'Staff Humas']);
        factory(Position::class)->create(['position_name' => 'Pembantu Humas']);
        factory(Position::class)->create(['position_name' => 'Pengembang Sistem Informasi dan Komunikasi']);

        factory(Position::class)->create(['position_name' => 'Kepala Sub Bidang Tata Usaha']);
        factory(Position::class)->create(['position_name' => 'Bendahara BOS']);
        factory(Position::class)->create(['position_name' => 'Bendahara Umum']);
        factory(Position::class)->create(['position_name' => 'Pembantu Keuangan']);
        factory(Position::class)->create(['position_name' => 'Pengelola Keuangan']);
        factory(Position::class)->create(['position_name' => 'Pengelola Laboratorium']);
        factory(Position::class)->create(['position_name' => 'Operator DAPODIK']);
        factory(Position::class)->create(['position_name' => 'Adm. Inventaris Barang']);
        factory(Position::class)->create(['position_name' => 'Adm. Kepegawaian']);
        factory(Position::class)->create(['position_name' => 'Adm. Kesiswaan']);
        factory(Position::class)->create(['position_name' => 'Adm. Kurikulum']);
        factory(Position::class)->create(['position_name' => 'Adm. Perpustakaan']);
        factory(Position::class)->create(['position_name' => 'Adm. Umum']);
        factory(Position::class)->create(['position_name' => 'Adm. Keuangan']);
        factory(Position::class)->create(['position_name' => 'Adm. Laboatorium']);
        factory(Position::class)->create(['position_name' => 'IT']);
        factory(Position::class)->create(['position_name' => 'Satpam']);
        factory(Position::class)->create(['position_name' => 'Caraka']);
        factory(Position::class)->create(['position_name' => 'Pengemudi']);

        factory(Position::class)->create(['position_name' => 'Guru Madya']);
        factory(Position::class)->create(['position_name' => 'Guru Muda']);
        factory(Position::class)->create(['position_name' => 'Guru Pertama']);
        factory(Position::class)->create(['position_name' => 'Guru Bahasa Indonesia']);
        factory(Position::class)->create(['position_name' => 'Guru Bahasa Sunda']);
        factory(Position::class)->create(['position_name' => 'Guru Bahasa Jepang']);
        factory(Position::class)->create(['position_name' => 'Guru PAI']);
        factory(Position::class)->create(['position_name' => 'Guru Agama Kristen']);
        factory(Position::class)->create(['position_name' => 'Guru Matematika']);
        factory(Position::class)->create(['position_name' => 'Guru Seni Tari']);
        factory(Position::class)->create(['position_name' => 'Guru BK']);

        factory(Position::class)->create(['position_name' => 'Siswa Aktif']);
        factory(Position::class)->create(['position_name' => 'Alumni']);
        

    }
}
