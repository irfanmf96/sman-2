<?php

namespace App\Http\Controllers;
use App\Kelas;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function index()
    {
        $kelas = Kelas::where('active',1)->where('tenant_id',Auth::user()->person->tenant_id)->get();
        return view('admin.kelas.index',compact('kelas'));
    }

    public function create()
    {
        return view('admin.kelas.create');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $this->validate($request,[
            'name_class' => 'required|min:2',
            'grade_class' => 'required|numeric'
        ]);
        try{
            $kelas = new Kelas;

            $kelas->grade = $request['grade_class'];
            $kelas->name = $request['name_class'];
            $kelas->tenant_id = Auth::user()->person->tenant_id;

            $kelas->save();
            DB::commit();
            return redirect()->back()->with('success','Data berhasil disimpan');
        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('warning',$e->getMessage());
        }
    }

    public function edit($id)
    {
        $model = Kelas::findOrFail($id);
        return $model;
        return view('admin.kelas.update',compact('model'));
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try{
            $kelas = Kelas::findOrFail($request['id']);

            $kelas->grade = $request['grade_class'];
            $kelas->name = $request['name_class'];

            $kelas->save();
            DB::commit();
            return redirect()->back()->with('success','Data berhasil disimpan');
        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('warning',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $kelas = Kelas::findOrFail($id);
            $kelas->active = 0;
            $kelas->save();
            DB::commit();
            return redirect()->back()->with('info','Data berhasil dihapus.');
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with('danger',$e->getMessage());
        }
    }
}
