<?php

namespace App\Http\Controllers;

use App\Sex;
use App\Religion;
use App\Person;
use App\User;
use App\Position;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StaffController extends Controller
{
    public function index()
    {
        $staff = Person::where('role_id',3)->where('active',1)->where('tenant_id',Auth::user()->person->tenant_id)->get();
        return view('admin.staff.index',compact('staff'));
    }

    public function create()
    {
        $sex = Sex::get();
        $religion = Religion::get();
        $position = Position::get();
        return view('admin.staff.create',compact('sex','religion','position'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nomor_induk_staff' => 'required|numeric|min:3',
            'nama_depan' => 'required|min:2',
            'tempat_lahir' => 'required|min:2',
            'tanggal_lahir'=>'required|date',
            'jenis_kelamin'=>'required',
            'agama'=>'required',
            'email' => 'required|email',
            'mobile_phone'=>'required|min:10',
            'jabatan'=>'required',
        ]);

        DB::beginTransaction();

        try{
            $user = new User;

            $user->email = $request['email'];
            $user->password = Hash::make('123456');

            $user->save();

            $staff = new Person;

            $staff->pin = $request['nomor_induk_staff'];
            $staff->first_name = $request['nama_depan'];
            $staff->last_name = $request['nama_belakang'];
            $staff->address = $request['alamat'];
            $staff->birth_place = $request['tempat_lahir'];
            $staff->birth_date = $request['tanggal_lahir'];
            $staff->email = $request['email'];
            $staff->mobile_phone = $request['mobile_phone'];
            $staff->sex_id = $request['jenis_kelamin'];
            $staff->religion_id = $request['agama'];
            $staff->position_id = $request['jabatan'];
            $staff->role_id = 3;
            $staff->user_id = $user->id;
            $staff->tenant_id = Auth::user()->person->tenant_id;

            if($request->hasFile('pas-foto')){
                $files       = $request['pas-foto'];
                $filenameOri = $files->hashName();
                $fileExt =  $files->extension();
                $fileName = Str::random(10);
                $url = URL::to('/').'/storage/staff/pas_foto/'.$fileName.".".$fileExt;

                $staff->pas_foto = $url;

                Storage::disk('public')->put('staff/pas_foto/'.$fileName.".".$fileExt, file_get_contents($files->getRealPath()));
            }

            $staff->save();

            DB::commit();

            return redirect()->back()->with('success','Data berhasil disimpan');;

        }catch(Exception $e){
            DB::rollBack();

            return redirect()->back()->with('danger',$e->getMessage());;
        }
    }

    public function edit($id)
    {
        $model = Person::findorFail($id);
        $sex = Sex::get();
        $religion = Religion::get();
        $position = Position::get();

        return view('admin.staff.update',compact('model','sex','religion','position'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'nomor_induk_staff' => 'required|numeric|min:3',
            'nama_depan' => 'required|min:2',
            'tempat_lahir' => 'required|min:2',
            'tanggal_lahir'=>'required|date',
            'jenis_kelamin'=>'required',
            'agama'=>'required',
            'email' => 'required|email',
            'mobile_phone'=>'required|min:10',
            'jabatan'=>'required',
        ]);

        DB::beginTransaction();
        try{
            $staff = Person::findorFail($request['staff_id']);
            $staff->pin = $request['nomor_induk_staff'];
            $staff->first_name = $request['nama_depan'];
            $staff->last_name = $request['nama_belakang'];
            $staff->address = $request['alamat'];
            $staff->birth_place = $request['tempat_lahir'];
            $staff->birth_date = $request['tanggal_lahir'];
            $staff->email = $request['email'];
            $staff->mobile_phone = $request['mobile_phone'];
            $staff->sex_id = $request['jenis_kelamin'];
            $staff->religion_id = $request['agama'];
            $staff->position_id = $request['jabatan'];

            if($request->hasFile('pas-foto')){
                $files       = $request['pas-foto'];
                $filenameOri = $files->hashName();
                $fileExt =  $files->extension();
                $fileName = Str::random(10);
                $url = URL::to('/').'/storage/staff/pas_foto/'.$fileName.".".$fileExt;

                $staff->pas_foto = $url;

                Storage::disk('public')->put('staff/pas_foto/'.$fileName.".".$fileExt, file_get_contents($files->getRealPath()));
            }

            $staff->save();

            DB::commit();
            return redirect()->back()->with('success','Data berhasil disimpan');
        }catch(Exception $e){
            DB::rollBack();

            return redirect()->back()->with('danger',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $staff = Person::findOrFail($id);
            if($staff->role_id==3){
                $staff->active = 0;
            }else{
                return redirect()->back()->with('warning','Maaf, terjadi kesalahan.');
            }
            $staff->save();
            DB::commit();
            return redirect()->back()->with('info','Data berhasil dihapus.');
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with('danger',$e->getMessage());
        }
    }
}
