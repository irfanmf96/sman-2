<?php

namespace App\Http\Controllers;

use App\Person;
use App\User;
use App\Religion;
use App\Sex;
use App\Position;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $student = Person::where('role_id',4)->where('active',1)->where('tenant_id',Auth::user()->person->tenant_id)->get();
        return view('admin.student.index',compact('student'));
    }

    public function create()
    {
        $religion = Religion::get();
        $sex = Sex::get();
        $position = Position::get();
        return view('admin.student.create',compact('religion','sex','position'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nomor_induk_siswa' => 'required|numeric|min:3',
            'nama_depan' => 'required|min:2',
            'tempat_lahir' => 'required|min:2',
            'tanggal_lahir'=>'required|date',
            'jenis_kelamin'=>'required',
            'agama'=>'required',
            'email' => 'required|email',
            'mobile_phone'=>'required|min:10',
        ]);

        DB::beginTransaction();

        try{
            $user = new User;

            $user->email = $request['email'];
            $user->password = Hash::make('123456');

            $user->save();

            $student = new Person;

            $student->pin = $request['nomor_induk_siswa'];
            $student->first_name = $request['nama_depan'];
            $student->last_name = $request['nama_belakang'];
            $student->address = $request['alamat'];
            $student->birth_place = $request['tempat_lahir'];
            $student->birth_date = $request['tanggal_lahir'];
            $student->email = $request['email'];
            $student->mobile_phone = $request['mobile_phone'];
            $student->sex_id = $request['jenis_kelamin'];
            $student->religion_id = $request['agama'];
            $student->role_id = 4;
            $student->user_id = $user->id;
            $student->tenant_id = Auth::user()->person->tenant_id;
            
            $student->save();

            DB::commit();

            return redirect()->back()->with('success','Data berhasil disimpan');
        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('danger',$e->getMessage());
        }
    }

    public function edit($id)
    {
        $model = Person::findOrFail($id);
        $sex = Sex::get();
        $religion = Religion::get();
        $position = Position::get();
        return view('admin.student.update', compact('model','sex','religion','position'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'nomor_induk_siswa' => 'required|numeric|min:3',
            'nama_depan' => 'required|min:2',
            'tempat_lahir' => 'required|min:2',
            'tanggal_lahir'=>'required|date',
            'jenis_kelamin'=>'required',
            'agama'=>'required',
            'email' => 'required|email',
            'mobile_phone'=>'required|min:10',
        ]);
        
        DB::beginTransaction();

        try{

            $student = Person::findOrFail($request['murid_id']);

            $student->pin = $request['nomor_induk_siswa'];
            $student->first_name = $request['nama_depan'];
            $student->last_name = $request['nama_belakang'];
            $student->address = $request['alamat'];
            $student->birth_place = $request['tempat_lahir'];
            $student->birth_date = $request['tanggal_lahir'];
            $student->email = $request['email'];
            $student->mobile_phone = $request['mobile_phone'];
            $student->sex_id = $request['jenis_kelamin'];
            $student->religion_id = $request['agama'];

            $student->save();

            DB::commit();

            return redirect()->back()->with('success','Data berhasil disimpan');
        }catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->with('danger',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();

        try{
            $student = Person::findOrFail($id);
            if($student->role_id==4){
                $student->active = 0;
            }else{
                return redirect()->back()->with('warning','Failed to delete data.');
            }
            $student->save();
            DB::commit();
            return redirect()->back()->with('info','Data successfully deleted.');
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with('danger',$e->getMessage());
        }
    }

}
