<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
Use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::get();
        return view('admin.blog.index',compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $blog = new Blog;
            $blog->title = $request['title'];
            $blog->content = $request['content'];

            if($request->hasFile('header-image')){
                $files       = $request['header-image'];
                $filenameOri = $files->hashName();
                $fileExt =  $files->extension();
                $fileName = Str::random(10);
                $url = URL::to('/').'/storage/blog/header_image/'.$fileName.".".$fileExt;

                $blog->header_image = $url;

                Storage::disk('public')->put('blog/header_image/'.$fileName.".".$fileExt, file_get_contents($files->getRealPath()));
            }

            if($request->hasFile('attachment')){
                $files       = $request['attachment'];
                $filenameOri = $files->hashName();
                $fileExt =  $files->extension();
                $fileName = str::random(10);
                $url = URL::to('/').'/storage/blog/attachment/'.$fileName.".".$fileExt;

                $blog->attachment = $url;
                $blog->attachment_original_name = $filenameOri;

                Storage::disk('public')->put('blog/attachment/'.$fileName.".".$fileExt, file_get_contents($files->getRealPath()));
            }

            $blog->save();

            DB::commit();
            return redirect()->back()->with('success','Data berhasil disimpan');
        }  catch(\Illuminate\Database\QueryException $ex){
            DB::rollback();
            return redirect()->back()->with('danger',$ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Blog::findOrFail($id);
        return view('admin.blog.update',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $blog = Blog::findOrFail($request['id']);
            $blog->title = $request['title'];
            $blog->content = $request['content'];

            if($request->hasFile('header-image')){
                $files       = $request['header-image'];
                $filenameOri = $files->hashName();
                $fileExt =  $files->extension();
                $fileName = Str::random(10);
                $url = URL::to('/').'/storage/blog/header_image/'.$fileName.".".$fileExt;

                $blog->header_image = $url;

                Storage::disk('public')->put('blog/header_image/'.$fileName.".".$fileExt, file_get_contents($files->getRealPath()));
            }

            if($request->hasFile('attachment')){
                $files       = $request['attachment'];
                $filenameOri = $files->hashName();
                $fileExt =  $files->extension();
                $fileName = str::random(10);
                $url = URL::to('/').'/storage/blog/attachment/'.$fileName.".".$fileExt;

                $blog->attachment = $url;
                $blog->attachment_original_name = $filenameOri;

                Storage::disk('public')->put('blog/attachment/'.$fileName.".".$fileExt, file_get_contents($files->getRealPath()));
            }

            $blog->save();

            DB::commit();
            return redirect()->back()->with('success','Data berhasil disimpan');
        }  catch(\Illuminate\Database\QueryException $ex){
            DB::rollback();
            return redirect()->back()->with('danger',$ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
