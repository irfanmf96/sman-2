<?php

namespace App\Http\Controllers;

use App\class_history;
use Illuminate\Http\Request;

class ClassHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\class_history  $class_history
     * @return \Illuminate\Http\Response
     */
    public function show(class_history $class_history)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\class_history  $class_history
     * @return \Illuminate\Http\Response
     */
    public function edit(class_history $class_history)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\class_history  $class_history
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, class_history $class_history)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\class_history  $class_history
     * @return \Illuminate\Http\Response
     */
    public function destroy(class_history $class_history)
    {
        //
    }
}
