<?php

namespace App\Http\Controllers;

use App\Blog;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
        
    //     return view('home',compact('blog'));
    // }

    public function index2()
    {
        $blog = Blog::where('active',1)->limit(6)->orderBy('created_at','desc')->get();
        return view('BizPage/index',compact('blog'));
    }

    public function news2()
    {
        $blog = Blog::where('active',1)->orderBy('created_at','desc')->paginate(5);
        return view('BizPage/news',compact('blog'));
    }

    public function gallery2()
    {
        return view('BizPage/gallery');
    }

    // public function login()
    // {
    //     return view('auth.login');
    // }
    public function students_alumni_list()
    {
        return view('students_alumni_list');
    }

    public function news()
    {
        $blog = Blog::where('active',1)->orderBy('created_at','desc')->paginate(5);
        return view('news',compact('blog'));
    }

    public function history()
    {
        return view('history');
    }

    public function vision_mission()
    {
        return view('vision_mission');
    }

    public function facility()
    {
        return view('facility');
    }

    public function contact()
    {
        return view('contact');
    }

    public function education_staff()
    {
        return view('education_staff');
    }

    public function news_detail()
    {
        return view('news_detail');
    }

    public function osis()
    {
        return view('osis');
    }

    public function mpk()
    {
        return view('mpk');
    }

    public function excul()
    {
        return view('Extracurricular');
    }

    public function gallery()
    {
        return view('gallery');
    }
}
