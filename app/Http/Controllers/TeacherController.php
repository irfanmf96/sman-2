<?php

namespace App\Http\Controllers;

use App\Sex;
use App\Religion;
use App\Person;
use App\User;
use App\Position;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TeacherController extends Controller
{
    public function index()
    {
        $teacher = Person::where('role_id',2)->where('active',1)->where('tenant_id',Auth::user()->person->tenant_id)->get();
        return view('admin.teacher.index',compact('teacher'));
    }

    public function create()
    {
        $sex = Sex::get();
        $religion = Religion::get();
        $position = Position::get();
        return view('admin.teacher.create',compact('sex','religion','position'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nomor_induk_guru' => 'required|numeric|min:3',
            'nama_depan' => 'required|min:2',
            'tempat_lahir' => 'required|min:2',
            'tanggal_lahir'=>'required|date',
            'jenis_kelamin'=>'required',
            'agama'=>'required',
            'email' => 'required|email',
            'mobile_phone'=>'required|min:10',
            'jabatan'=>'required',
        ]);

        DB::beginTransaction();

        try{
            $user = new User;

            $user->email = $request['email'];
            $user->password = Hash::make('123456');

            $user->save();

            $teacher = new Person;

            $teacher->pin = $request['nomor_induk_guru'];
            $teacher->first_name = $request['nama_depan'];
            $teacher->last_name = $request['nama_belakang'];
            $teacher->address = $request['alamat'];
            $teacher->birth_place = $request['tempat_lahir'];
            $teacher->birth_date = $request['tanggal_lahir'];
            $teacher->email = $request['email'];
            $teacher->mobile_phone = $request['mobile_phone'];
            $teacher->sex_id = $request['jenis_kelamin'];
            $teacher->religion_id = $request['agama'];
            $teacher->position_id = $request['jabatan'];
            $teacher->role_id = 2;
            $teacher->user_id = $user->id;
            $teacher->tenant_id = Auth::user()->person->tenant_id;

            if($request->hasFile('pas-foto')){
                $files       = $request['pas-foto'];
                $filenameOri = $files->hashName();
                $fileExt =  $files->extension();
                $fileName = Str::random(10);
                $url = URL::to('/').'/storage/teacher/pas_foto/'.$fileName.".".$fileExt;

                $teacher->pas_foto = $url;

                Storage::disk('public')->put('teacher/pas_foto/'.$fileName.".".$fileExt, file_get_contents($files->getRealPath()));
            }

            $teacher->save();

            DB::commit();

            return redirect()->back()->with('success','Data berhasil disimpan');;

        }catch(Exception $e){
            DB::rollBack();

            return redirect()->back()->with('danger',$e->getMessage());;
        }
    }
    public function edit($id)
    {
        $model = Person::findorFail($id);
        $sex = Sex::get();
        $religion = Religion::get();
        $position = Position::get();

        return view('admin.teacher.update',compact('model','sex','religion','position'));

    }
    public function update(Request $request)
    {
        $request->validate([
            'nomor_induk_guru' => 'required|numeric|min:3',
            'nama_depan' => 'required|min:2',
            'tempat_lahir' => 'required|min:2',
            'tanggal_lahir'=>'required|date',
            'jenis_kelamin'=>'required',
            'agama'=>'required',
            'email' => 'required|email',
            'mobile_phone'=>'required|min:10',
            'jabatan'=>'required',
        ]);

        DB::beginTransaction();
        try{
            $teacher = Person::findorFail($request['guru_id']);
            $teacher->pin = $request['nomor_induk_guru'];
            $teacher->first_name = $request['nama_depan'];
            $teacher->last_name = $request['nama_belakang'];
            $teacher->address = $request['alamat'];
            $teacher->birth_place = $request['tempat_lahir'];
            $teacher->birth_date = $request['tanggal_lahir'];
            $teacher->email = $request['email'];
            $teacher->mobile_phone = $request['mobile_phone'];
            $teacher->sex_id = $request['jenis_kelamin'];
            $teacher->religion_id = $request['agama'];
            $teacher->position_id = $request['jabatan'];

            if($request->hasFile('pas-foto')){
                $files       = $request['pas-foto'];
                $filenameOri = $files->hashName();
                $fileExt =  $files->extension();
                $fileName = Str::random(10);
                $url = URL::to('/').'/storage/teacher/pas_foto/'.$fileName.".".$fileExt;

                $teacher->pas_foto = $url;

                Storage::disk('public')->put('teacher/pas_foto/'.$fileName.".".$fileExt, file_get_contents($files->getRealPath()));
            }

            $teacher->save();

            DB::commit();
            return redirect()->back()->with('success','Data berhasil disimpan');
        }catch(Exception $e){
            DB::rollBack();

            return redirect()->back()->with('danger',$e->getMessage());
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            $teacher = Person::findOrFail($id);
            if($teacher->role_id==2){
                $teacher->active = 0;
            }else{
                return redirect()->back()->with('warning','Maaf, terjadi kesalahan.');
            }
            $teacher->save();
            DB::commit();
            return redirect()->back()->with('info','Data berhasil dihapus.');
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with('danger',$e->getMessage());
        }
    }
}
