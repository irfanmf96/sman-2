<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function showLoginForm()
    {
        return view('BizPage.index');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginAdmin(Request $request){
        $result = $this->login($request);
        if(Auth::check()){
            return redirect('/home');
        }else{
            return redirect()->back()->withInput()->with('failed_login',$result['message'][0]);
        }
    }

    public function login(Request $request)
    {
        $result  = [];
        $data = $request->only('email','password');
        try{
            $users = User::where('email',$data['email'])->where('active',1)->first();

            if(!empty($users)){
                if(Auth::attempt(['id' => $users['id'],'password'=> $data['password']])){
                    if(Auth::user()->active!=1){
                        $message = 'Account no longer active';
                    }else {
                        $user = Auth::user();
                        $result = [
                            'token' => $user->createToken('MyApp')->accessToken
                        ];
                        $message = "you are successfully login";
                    }
                }else {
                    $message = "Wrong Password";
                }
            }else {
                $message = "User not found";
            }
        }catch(\Exception $e){
            return [
                'message' => $e->getMessage()
            ];
        }
        return [
            'message' => [
                0 => $message
            ],
            'records' => $result
        ];
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
