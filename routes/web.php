<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'HomeController@index2')->name('home');
Route::post('login_admin','Auth\LoginController@loginAdmin')->name('login-admin');
Route::get('/students_alumni_list', 'HomeController@students_alumni_list')->name('students_alumni_list');

Route::prefix('news')->group(function(){
    Route::get('beta', 'HomeController@news2')->name('betaNews');
    Route::get('/', 'HomeController@news')->name('news');
    Route::get('detail', 'HomeController@news_detail')->name('news_detail');
});  

Route::prefix('profile')->group(function(){
    Route::get('history', 'HomeController@history')->name('history');
    Route::get('vision_mission', 'HomeController@vision_mission')->name('vision_mission');
    Route::get('facility', 'HomeController@facility')->name('facility');
    Route::get('contact', 'HomeController@contact')->name('contact');
    Route::get('education_staff', 'HomeController@education_staff')->name('education_staff');
});

Route::prefix('students')->group(function(){
    Route::get('osis', 'HomeController@osis')->name('osis');
    Route::get('mpk', 'HomeController@mpk')->name('mpk');
    Route::get('extracurricular', 'HomeController@excul')->name('excul');
});

Route::prefix('gallery')->group(function(){
    Route::get('beta', 'HomeController@gallery2')->name('betaGallery');
    Route::get('/', 'HomeController@gallery')->name('gallery');
});

Route::group([ 'middleware' => 'auth'], function () {
    Route::get('home','DashboardController@index')->name('dashboard');
    Route::prefix('student')->group(function(){
        Route::get('/','StudentController@index')->name('student');
        Route::get('create','StudentController@create')->name('student.create');
        Route::post('store','StudentController@store')->name('student.store');
        Route::get('edit/{id}','StudentController@edit')->name('student.edit');
        Route::post('update','StudentController@update')->name('student.update');
        Route::get('delete/{id}','StudentController@destroy')->name('student.destroy');
    });
    Route::prefix('teacher')->group(function(){
        Route::get('/','TeacherController@index')->name('teacher');
        Route::get('create','TeacherController@create')->name('teacher.create');
        Route::post('store','TeacherController@store')->name('teacher.store');
        Route::get('edit/{id}','TeacherController@edit')->name('teacher.edit');
        Route::post('update','TeacherController@update')->name('teacher.update');
        Route::get('delete/{id}','TeacherController@destroy')->name('teacher.destroy');
    });

    Route::prefix('staff')->group(function(){
        Route::get('/','StaffController@index')->name('staff');
        Route::get('create','StaffController@create')->name('staff.create');
        Route::post('store','StaffController@store')->name('staff.store');
        Route::get('edit/{id}','StaffController@edit')->name('staff.edit');
        Route::post('update','StaffController@update')->name('staff.update');
        Route::get('delete/{id}','StaffController@destroy')->name('staff.destroy');
    });

    Route::prefix('blog')->group(function(){
        Route::get('/','BlogController@index')->name('blog');
        Route::get('create','BlogController@create')->name('blog.create');
        Route::post('store','BlogController@store')->name('blog.store');
        Route::get('edit/{id}','BlogController@edit')->name('blog.edit');
        Route::post('update','BlogController@update')->name('blog.update');
        Route::get('delete/{id}','BlogController@destroy')->name('blog.destroy');
    });
});